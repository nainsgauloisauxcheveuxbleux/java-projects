/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import controler.Condition;
import controler.Creation;
import controler.Type_Query;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;
import model.Result_line;

/**
 * Card containing the main functions : research, add or modification of element in the system
 * 
 * @author arthur
 * @version 2.0
 */
public class Main_card extends JPanel implements ItemListener, ActionListener, FocusListener{
    
    //Display components
    //All cards
    private final JPanel cards;
    //Main panel
    private final JPanel main_panel = new JPanel();
    //Input panel
        private final JPanel input_panel = new JPanel();
    //Stat and Request buttons
            private final JPanel stats_and_r_panel = new JPanel();
            private final JButton stats_button = new JButton("Stats");
            private final JButton r_button = new JButton("Base requests");
    //Search or add or update buttons
            private final JPanel radio_buttons_panel_1 = new JPanel();
            private final JLabel want_to_lbl = new JLabel("I want to :");
            private final JRadioButton search_rbut =new JRadioButton("Search");
            private final JRadioButton add_rbut =new JRadioButton("Add");
            private final JRadioButton update_rbut =new JRadioButton("Update");
    //Add patient or doctor or nurse buttons
            private final JPanel add_radio_buttons_panel = new JPanel();
            private final JRadioButton patient_rbut =new JRadioButton("Patient");
            private final JRadioButton doctor_rbut =new JRadioButton("Doctor");
            private final JRadioButton nurse_rbut =new JRadioButton("Nurse");
    //Table selection
            private final JLabel info_lbl = new JLabel("Information relative to :");
            private final JPanel checkbox_panel_table = new JPanel();
            private final JCheckBox chambre_ckbx =new JCheckBox("Chambre");
            private final JCheckBox docteur_ckbx =new JCheckBox("Docteur");
            private final JCheckBox employe_ckbx =new JCheckBox("Employe");
            private final JCheckBox hospitalisation_ckbx =new JCheckBox("Hospitalisation");
            private final JCheckBox infirmier_ckbx =new JCheckBox("Infirmier");
            private final JCheckBox malade_ckbx =new JCheckBox("Malade");
            private final JCheckBox service_ckbx =new JCheckBox("Service");
            private final JCheckBox soigne_ckbx =new JCheckBox("Soigne");
    //Criterias selection
            private final JLabel what_lbl = new JLabel("Criterias :");
            private final JPanel checkbox_panel_2 = new JPanel();      
            
            private final JCheckBox serv_ckbx =new JCheckBox("Service code");
            private final JCheckBox nroo_ckbx =new JCheckBox("Room n°");
            private final JCheckBox nemp_ckbx =new JCheckBox("Employee n°");
            private final JCheckBox beds_ckbx =new JCheckBox("Number of beds");
            private final JCheckBox spec_ckbx =new JCheckBox("Specialty");
            private final JCheckBox lemp_ckbx =new JCheckBox("Employee last name");
            private final JCheckBox femp_ckbx =new JCheckBox("Employee first name");
            private final JCheckBox aemp_ckbx =new JCheckBox("Employee address");
            private final JCheckBox pemp_ckbx =new JCheckBox("Employee phone n°");
            private final JCheckBox npat_ckbx =new JCheckBox("Patient n°");
            private final JCheckBox nbed_ckbx =new JCheckBox("Bed n°");
            private final JCheckBox rota_ckbx =new JCheckBox("Rotation");
            private final JCheckBox sala_ckbx =new JCheckBox("Salary");
            private final JCheckBox lpat_ckbx =new JCheckBox("Patient last name");
            private final JCheckBox fpat_ckbx =new JCheckBox("Patient first name");
            private final JCheckBox apat_ckbx =new JCheckBox("Patient address");
            private final JCheckBox ppat_ckbx =new JCheckBox("Patient phone n°");
            private final JCheckBox mutu_ckbx =new JCheckBox("Mutual");
            private final JCheckBox nser_ckbx =new JCheckBox("Service name");
            private final JCheckBox buil_ckbx =new JCheckBox("Building");
            private final JCheckBox dire_ckbx =new JCheckBox("Director");
    //Different textfield for different type       
            private final JPanel values_to_enter_panel = new JPanel();
            private final JPanel patient_values_to_enter_panel = new JPanel();
            private final JPanel doctor_values_to_enter_panel = new JPanel();
            private final JPanel nurse_values_to_enter_panel = new JPanel();
            
            private final JPanel serv_panel = new JPanel();
            private final JPanel nroo_panel =new JPanel();
            private final JPanel nemp_panel =new JPanel();
            private final JPanel beds_panel =new JPanel();
            private final JPanel spec_panel =new JPanel();
            private final JPanel lemp_panel =new JPanel();
            private final JPanel femp_panel =new JPanel();
            private final JPanel aemp_panel =new JPanel();
            private final JPanel pemp_panel =new JPanel();
            private final JPanel npat_panel =new JPanel();
            private final JPanel nbed_panel =new JPanel();
            private final JPanel rota_panel =new JPanel();
            private final JPanel sala_panel =new JPanel();
            private final JPanel lpat_panel =new JPanel();
            private final JPanel fpat_panel =new JPanel();
            private final JPanel apat_panel =new JPanel();
            private final JPanel ppat_panel =new JPanel();
            private final JPanel mutu_panel =new JPanel();
            private final JPanel nser_panel =new JPanel();
            private final JPanel buil_panel =new JPanel();
            private final JPanel dire_panel =new JPanel();
            
            private final JLabel serv_lbl = new JLabel("Service code");
            private final JLabel nroo_lbl =new JLabel("Room n°");
            private final JLabel nemp_lbl =new JLabel("Employee n°");
            private final JLabel beds_lbl =new JLabel("Number of beds");
            private final JLabel spec_lbl =new JLabel("Specialty");
            private final JLabel lemp_lbl =new JLabel("Employee last name");
            private final JLabel femp_lbl =new JLabel("Employee first name");
            private final JLabel aemp_lbl =new JLabel("Employee address");
            private final JLabel pemp_lbl =new JLabel("Employee phone n°");
            private final JLabel npat_lbl =new JLabel("Patient n°");
            private final JLabel nbed_lbl =new JLabel("Bed n°");
            private final JLabel rota_lbl =new JLabel("Rotation");
            private final JLabel sala_lbl =new JLabel("Salary");
            private final JLabel lpat_lbl =new JLabel("Patient last name");
            private final JLabel fpat_lbl =new JLabel("Patient first name");
            private final JLabel apat_lbl =new JLabel("Patient address");
            private final JLabel ppat_lbl =new JLabel("Patient phone number");
            private final JLabel mutu_lbl =new JLabel("Mutual");
            private final JLabel nser_lbl =new JLabel("Service name");
            private final JLabel buil_lbl =new JLabel("Building");
            private final JLabel dire_lbl =new JLabel("Director");
            
            private final JTextField serv_txt =new JTextField("Service code");
            private final JTextField nroo_txt =new JTextField("Room n°");
            private final JTextField nemp_txt =new JTextField("Employee n°");
            private final JTextField beds_txt =new JTextField("Number of beds");
            private final JTextField spec_txt =new JTextField("Specialty");
            private final JTextField lemp_txt =new JTextField("Employee last name");
            private final JTextField femp_txt =new JTextField("Employee first name");
            private final JTextField aemp_txt =new JTextField("Employee address");
            private final JTextField pemp_txt =new JTextField("Employee phone n°");
            private final JTextField npat_txt =new JTextField("Patient n°");
            private final JTextField nbed_txt =new JTextField("Bed n°");
            private final JTextField rota_txt =new JTextField("Rotation");
            private final JTextField sala_txt =new JTextField("Salary");
            private final JTextField lpat_txt =new JTextField("Patient last name");
            private final JTextField fpat_txt =new JTextField("Patient first name");
            private final JTextField apat_txt =new JTextField("Patient address");
            private final JTextField ppat_txt =new JTextField("Patient phone");
            private final JTextField mutu_txt =new JTextField("Mutual");
            private final JTextField nser_txt =new JTextField("Service name");
            private final JTextField buil_txt =new JTextField("Building");
            private final JTextField dire_txt =new JTextField("Director");
    //Fixing size problem       
            private final JPanel type_and_condition_panel = new JPanel();
            private final JComboBox type_cbx = new JComboBox(){
                @Override
                    public Dimension getMaximumSize() {
                        Dimension max = super.getMaximumSize();
                        max.height = getPreferredSize().height;
                        return max;
                    }
            };
            private final JTextField txt_to_srch = new JTextField("Precise your search here"){
                @Override
                    public Dimension getMaximumSize() {
                        Dimension max = super.getMaximumSize();
                        max.height = getPreferredSize().height;
                        return max;
                    }
            };
    //Execute request button
    private final JPanel done_panel = new JPanel();
        private final JButton done_button = new JButton("Done");
    private JPanel results_panel = new JPanel();
    
    //Fields and tables to take into accouny in the search
    ArrayList<String> tables = new ArrayList<String>();
    ArrayList<String> fields = new ArrayList<String>();
    ArrayList<String> values = new ArrayList<String>();
    ArrayList<String> set = new ArrayList<String>();
    
    //Variables for the search
    private Type_Query statement_type;
    ArrayList<Result_line> raw_result;
    Results_card res_card;

    
    /**
     * Initialize all the buttons in a clear UX design
     * 
     * @param _cards
     * 
     * @author arthur
     */
    public Main_card(JPanel _cards){
        super();
        cards = _cards;
        stats_and_r_panel.add(stats_button);
        stats_and_r_panel.add(r_button);
        
        ButtonGroup group = new ButtonGroup();
        group.add(search_rbut);
        search_rbut.setSelected(true);
        group.add(add_rbut);
        group.add(update_rbut);
        
        ButtonGroup group_2 = new ButtonGroup();
        group_2.add(patient_rbut);
        patient_rbut.setSelected(true);
        group_2.add(doctor_rbut);
        group_2.add(nurse_rbut);
        
        radio_buttons_panel_1.setLayout(new BoxLayout(radio_buttons_panel_1, BoxLayout.LINE_AXIS));
        radio_buttons_panel_1.add(want_to_lbl);
        radio_buttons_panel_1.add(search_rbut);
        radio_buttons_panel_1.add(add_rbut);
        radio_buttons_panel_1.add(update_rbut);
        
        add_radio_buttons_panel.setLayout(new BoxLayout(add_radio_buttons_panel, BoxLayout.LINE_AXIS));
        add_radio_buttons_panel.add(patient_rbut);
        add_radio_buttons_panel.add(doctor_rbut);
        add_radio_buttons_panel.add(nurse_rbut);
        
        //Set the layout for the checkbox panel
        checkbox_panel_2.setLayout(new GridLayout(0,7));
        //adding the checkboxes to the panel
        checkbox_panel_2.add(serv_ckbx);
        checkbox_panel_2.add(nroo_ckbx);
        checkbox_panel_2.add(nemp_ckbx);
        checkbox_panel_2.add(beds_ckbx);
        checkbox_panel_2.add(spec_ckbx);
        checkbox_panel_2.add(lemp_ckbx);
        checkbox_panel_2.add(femp_ckbx);
        checkbox_panel_2.add(aemp_ckbx);
        checkbox_panel_2.add(pemp_ckbx);
        checkbox_panel_2.add(npat_ckbx);
        checkbox_panel_2.add(nbed_ckbx);
        checkbox_panel_2.add(rota_ckbx);
        checkbox_panel_2.add(sala_ckbx);
        checkbox_panel_2.add(lpat_ckbx);
        checkbox_panel_2.add(fpat_ckbx);
        checkbox_panel_2.add(apat_ckbx);
        checkbox_panel_2.add(ppat_ckbx);
        checkbox_panel_2.add(mutu_ckbx);
        checkbox_panel_2.add(nser_ckbx);
        checkbox_panel_2.add(buil_ckbx);
        checkbox_panel_2.add(dire_ckbx);  
        
        //Set layout for the data input panel
        values_to_enter_panel.setLayout(new GridLayout(0,7));
        serv_panel.setLayout(new GridLayout(0,1));
        nroo_panel.setLayout(new GridLayout(0,1));
        nemp_panel.setLayout(new GridLayout(0,1));
        beds_panel.setLayout(new GridLayout(0,1));
        spec_panel.setLayout(new GridLayout(0,1));
        lemp_panel.setLayout(new GridLayout(0,1));
        femp_panel.setLayout(new GridLayout(0,1));
        aemp_panel.setLayout(new GridLayout(0,1));
        pemp_panel.setLayout(new GridLayout(0,1));
        npat_panel.setLayout(new GridLayout(0,1));
        nbed_panel.setLayout(new GridLayout(0,1));
        rota_panel.setLayout(new GridLayout(0,1));
        sala_panel.setLayout(new GridLayout(0,1));
        lpat_panel.setLayout(new GridLayout(0,1));
        fpat_panel.setLayout(new GridLayout(0,1));
        apat_panel.setLayout(new GridLayout(0,1));
        ppat_panel.setLayout(new GridLayout(0,1));
        mutu_panel.setLayout(new GridLayout(0,1));
        nser_panel.setLayout(new GridLayout(0,1));
        buil_panel.setLayout(new GridLayout(0,1));
        dire_panel.setLayout(new GridLayout(0,1));
        
        serv_panel.add(serv_lbl);   serv_panel.add(serv_txt);
        nroo_panel.add(nroo_lbl);   nroo_panel.add(nroo_txt);
        nemp_panel.add(nemp_lbl);   nemp_panel.add(nemp_txt);
        beds_panel.add(beds_lbl);   beds_panel.add(beds_txt);
        spec_panel.add(spec_lbl);   spec_panel.add(spec_txt);
        lemp_panel.add(lemp_lbl);   lemp_panel.add(lemp_txt);
        femp_panel.add(femp_lbl);   femp_panel.add(femp_txt);
        aemp_panel.add(aemp_lbl);   aemp_panel.add(aemp_txt);
        pemp_panel.add(pemp_lbl);   pemp_panel.add(pemp_txt);
        npat_panel.add(npat_lbl);   npat_panel.add(npat_txt);
        nbed_panel.add(nbed_lbl);   nbed_panel.add(nbed_txt);
        rota_panel.add(rota_lbl);   rota_panel.add(rota_txt);
        sala_panel.add(sala_lbl);   sala_panel.add(sala_txt);
        lpat_panel.add(lpat_lbl);   lpat_panel.add(lpat_txt);
        fpat_panel.add(fpat_lbl);   fpat_panel.add(fpat_txt);
        apat_panel.add(apat_lbl);   apat_panel.add(apat_txt);
        ppat_panel.add(ppat_lbl);   ppat_panel.add(ppat_txt);
        mutu_panel.add(mutu_lbl);   mutu_panel.add(mutu_txt);
        nser_panel.add(nser_lbl);   nser_panel.add(nser_txt);
        buil_panel.add(buil_lbl);   buil_panel.add(buil_txt);
        dire_panel.add(dire_lbl);   dire_panel.add(dire_txt);
        
        patient_values_to_enter_panel.setLayout(new GridLayout(0,3));
        patient_values_to_enter_panel.add(fpat_panel);
        patient_values_to_enter_panel.add(lpat_panel);
        patient_values_to_enter_panel.add(apat_panel);
        patient_values_to_enter_panel.add(ppat_panel);
        patient_values_to_enter_panel.add(mutu_panel);
        patient_values_to_enter_panel.add(nemp_panel);
        patient_values_to_enter_panel.add(nroo_panel);
        patient_values_to_enter_panel.add(nbed_panel);
        patient_values_to_enter_panel.add(serv_panel);
        doctor_values_to_enter_panel.setLayout(new GridLayout(0,5));
        nurse_values_to_enter_panel.setLayout(new GridLayout(0,7));
        
        checkbox_panel_table.setLayout(new BoxLayout(checkbox_panel_table, BoxLayout.LINE_AXIS));
        checkbox_panel_table.add(chambre_ckbx);
        checkbox_panel_table.add(docteur_ckbx);
        checkbox_panel_table.add(employe_ckbx);
        checkbox_panel_table.add(hospitalisation_ckbx);
        checkbox_panel_table.add(infirmier_ckbx);
        checkbox_panel_table.add(malade_ckbx);
        checkbox_panel_table.add(service_ckbx);
        checkbox_panel_table.add(soigne_ckbx);
        
        type_and_condition_panel.setLayout(new BoxLayout(type_and_condition_panel, BoxLayout.LINE_AXIS));
        type_and_condition_panel.add(type_cbx);
        type_and_condition_panel.add(txt_to_srch);
        
        
        input_panel.setLayout(new BoxLayout(input_panel, BoxLayout.PAGE_AXIS));
        input_panel.add(stats_and_r_panel);
        input_panel.add(radio_buttons_panel_1);
        input_panel.add(info_lbl);
        input_panel.add(checkbox_panel_table);
        input_panel.add(what_lbl);
        input_panel.add(checkbox_panel_2);
        input_panel.add(type_and_condition_panel);
        done_panel.add(done_button);
        MutableComboBoxModel model;
        model = (MutableComboBoxModel)type_cbx.getModel();
        

    }
    /**
     * Add of the action listener
     * 
     * @author arthur
     */
    public void init(){
        
        search_rbut.addActionListener((ActionListener) this);
        add_rbut.addActionListener((ActionListener) this);
        update_rbut.addActionListener((ActionListener) this);
        
        patient_rbut.addActionListener((ActionListener) this);
        doctor_rbut.addActionListener((ActionListener) this);
        nurse_rbut.addActionListener((ActionListener) this);
        
        stats_button.addActionListener((ActionListener) this);
        r_button.addActionListener((ActionListener) this);
        done_button.addActionListener((ActionListener) this);
        txt_to_srch.addFocusListener((FocusListener) this);
        
        chambre_ckbx.addItemListener((ItemListener) this);
        docteur_ckbx.addItemListener((ItemListener) this);
        employe_ckbx.addItemListener((ItemListener) this);
        hospitalisation_ckbx.addItemListener((ItemListener) this);
        infirmier_ckbx.addItemListener((ItemListener) this);
        malade_ckbx.addItemListener((ItemListener) this);
        service_ckbx.addItemListener((ItemListener) this);
        soigne_ckbx.addItemListener((ItemListener) this);
        
        serv_ckbx.addItemListener((ItemListener) this);
        nroo_ckbx.addItemListener((ItemListener) this);
        nemp_ckbx.addItemListener((ItemListener) this);
        beds_ckbx.addItemListener((ItemListener) this);
        spec_ckbx.addItemListener((ItemListener) this);
        lemp_ckbx.addItemListener((ItemListener) this);
        femp_ckbx.addItemListener((ItemListener) this);
        aemp_ckbx.addItemListener((ItemListener) this);
        pemp_ckbx.addItemListener((ItemListener) this);
        npat_ckbx.addItemListener((ItemListener) this);
        nbed_ckbx.addItemListener((ItemListener) this);
        rota_ckbx.addItemListener((ItemListener) this);
        sala_ckbx.addItemListener((ItemListener) this);
        lpat_ckbx.addItemListener((ItemListener) this);
        fpat_ckbx.addItemListener((ItemListener) this);
        apat_ckbx.addItemListener((ItemListener) this);
        ppat_ckbx.addItemListener((ItemListener) this);
        mutu_ckbx.addItemListener((ItemListener) this);
        nser_ckbx.addItemListener((ItemListener) this);
        buil_ckbx.addItemListener((ItemListener) this);
        dire_ckbx.addItemListener((ItemListener) this);
        
        serv_txt.addFocusListener((FocusListener) this);
        nroo_txt.addFocusListener((FocusListener) this);
        nemp_txt.addFocusListener((FocusListener) this);
        beds_txt.addFocusListener((FocusListener) this);
        spec_txt.addFocusListener((FocusListener) this);
        lemp_txt.addFocusListener((FocusListener) this);
        femp_txt.addFocusListener((FocusListener) this);
        aemp_txt.addFocusListener((FocusListener) this);
        pemp_txt.addFocusListener((FocusListener) this);
        npat_txt.addFocusListener((FocusListener) this);
        nbed_txt.addFocusListener((FocusListener) this);
        rota_txt.addFocusListener((FocusListener) this);
        sala_txt.addFocusListener((FocusListener) this);
        fpat_txt.addFocusListener((FocusListener) this);
        lpat_txt.addFocusListener((FocusListener) this);
        apat_txt.addFocusListener((FocusListener) this);
        ppat_txt.addFocusListener((FocusListener) this);
        mutu_txt.addFocusListener((FocusListener) this);
        nser_txt.addFocusListener((FocusListener) this);
        buil_txt.addFocusListener((FocusListener) this);
        dire_txt.addFocusListener((FocusListener) this);
        
        main_panel.setLayout(new BoxLayout(main_panel, BoxLayout.PAGE_AXIS));
        main_panel.add(input_panel);
        main_panel.add(done_panel);
        main_panel.add(results_panel);
            
        this.add(main_panel);
        this.setVisible(true);
    }
    /**
     * Retrieval of the data necessary to the use of class Creation 
     * 
     * @param e 
     * @author arthur, Henri
     * source http://www.javased.com/?api=javax.swing.MutableComboBoxModel
     * source https://stackoverflow.com/questions/21642530/how-to-add-elements-in-jcombobox-dynamically
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        MutableComboBoxModel model;
        model = (MutableComboBoxModel)type_cbx.getModel();

        if (source == serv_ckbx) {
        
            //System.out.println("Touché");
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(serv_ckbx.getText());
                values_to_enter_panel.remove(serv_panel);
            }
            else{
                insertIntoCombo(type_cbx,serv_ckbx.getText());
                values_to_enter_panel.add(serv_panel);
                //System.out.println("coché");
            }
        }
        else if (source == nroo_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(nroo_ckbx.getText());
                values_to_enter_panel.remove(nroo_panel);
            }
            else{
                insertIntoCombo(type_cbx,nroo_ckbx.getText());
                values_to_enter_panel.add(nroo_panel);
            }     
        }
        else if (source == nemp_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(nemp_ckbx.getText());
                values_to_enter_panel.remove(nemp_panel);
            }
            else{
                insertIntoCombo(type_cbx,nemp_ckbx.getText());
                values_to_enter_panel.add(nemp_panel);
            }     
        }
        else if (source == beds_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(beds_ckbx.getText());
                values_to_enter_panel.remove(beds_panel);
            }
            else{
                insertIntoCombo(type_cbx,beds_ckbx.getText());
                values_to_enter_panel.add(beds_panel);
            }     
        }
        else if (source == spec_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(spec_ckbx.getText());
                values_to_enter_panel.remove(spec_panel);
            }
            else{
                insertIntoCombo(type_cbx,spec_ckbx.getText());
                values_to_enter_panel.add(spec_panel);
            }     
        }
        else if (source == lemp_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(lemp_ckbx.getText());
                values_to_enter_panel.remove(lemp_panel);
            }
            else{
                insertIntoCombo(type_cbx,lemp_ckbx.getText());
                values_to_enter_panel.add(lemp_panel);
            }     
        }
        else if (source == femp_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(femp_ckbx.getText());
                values_to_enter_panel.remove(femp_panel);
            }
            else{
                insertIntoCombo(type_cbx,femp_ckbx.getText());
                values_to_enter_panel.add(femp_panel);

            }     
        }
        else if (source == aemp_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(aemp_ckbx.getText());
                values_to_enter_panel.remove(aemp_panel);
            }
            else{
                insertIntoCombo(type_cbx,aemp_ckbx.getText());
                values_to_enter_panel.add(aemp_panel);
            }     
        }
        else if (source == pemp_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(pemp_ckbx.getText());
                values_to_enter_panel.remove(pemp_panel);
            }
            else{
                insertIntoCombo(type_cbx,pemp_ckbx.getText());
                values_to_enter_panel.add(pemp_panel);
            }     
        }
        else if (source == npat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(npat_ckbx.getText());
                values_to_enter_panel.remove(npat_panel);
            }
            else{
                insertIntoCombo(type_cbx,npat_ckbx.getText());
                values_to_enter_panel.add(npat_panel);
            }     
        }
        else if (source == nbed_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(nbed_ckbx.getText());
                values_to_enter_panel.remove(nbed_panel);
            }
            else{
                insertIntoCombo(type_cbx,nbed_ckbx.getText());
                values_to_enter_panel.add(nbed_panel);
            }     
        }
        else if (source == rota_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(rota_ckbx.getText());
                values_to_enter_panel.remove(rota_panel);
            }
            else{
                insertIntoCombo(type_cbx,rota_ckbx.getText());
                values_to_enter_panel.add(rota_panel);
            }     
        }
        else if (source == sala_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(sala_ckbx.getText());
                values_to_enter_panel.remove(sala_panel);
            }
            else{
                insertIntoCombo(type_cbx,sala_ckbx.getText());
                values_to_enter_panel.add(sala_panel);
            }     
        }
        else if (source == lpat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                 model.removeElement(lpat_ckbx.getText());
                values_to_enter_panel.remove(lpat_panel);
            }
            else{
                insertIntoCombo(type_cbx,lpat_ckbx.getText());
                values_to_enter_panel.add(lpat_panel);
            }     
        }
        else if (source == fpat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(lpat_ckbx.getText());
                values_to_enter_panel.remove(fpat_panel);
            }
            else{
                insertIntoCombo(type_cbx,lpat_ckbx.getText());
                values_to_enter_panel.add(fpat_panel);
            }
        }
        else if (source == fpat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(fpat_ckbx.getText());
            }
            else{
                insertIntoCombo(type_cbx,fpat_ckbx.getText());
            }     
        }
        else if (source == apat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(apat_ckbx.getText());
                values_to_enter_panel.remove(apat_panel);
            }
            else{
                insertIntoCombo(type_cbx,apat_ckbx.getText());
                values_to_enter_panel.add(apat_panel);
            }     
        }
        else if (source == ppat_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(ppat_ckbx.getText());
                values_to_enter_panel.remove(ppat_panel);
            }
            else{
                insertIntoCombo(type_cbx,ppat_ckbx.getText());
                values_to_enter_panel.add(ppat_panel);
            }     
        }
        else if (source == mutu_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(mutu_ckbx.getText());
                values_to_enter_panel.remove(mutu_panel);
            }
            else{
                insertIntoCombo(type_cbx,mutu_ckbx.getText());
                values_to_enter_panel.add(mutu_panel);
            }     
        }
        else if (source == nser_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(nser_ckbx.getText());
                values_to_enter_panel.remove(nser_panel);
            }
            else{
                insertIntoCombo(type_cbx,nser_ckbx.getText());
                values_to_enter_panel.add(nser_panel);
            }     
        }
        else if (source == buil_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(buil_ckbx.getText());
                values_to_enter_panel.remove(buil_panel);
            }
            else{
                insertIntoCombo(type_cbx,buil_ckbx.getText());
                values_to_enter_panel.add(buil_panel);
            }     
        }
        else if (source == dire_ckbx) {
            if (e.getStateChange() == ItemEvent.DESELECTED){
                model.removeElement(dire_ckbx.getText());
                values_to_enter_panel.remove(dire_panel);
            }
            else{
                insertIntoCombo(type_cbx,dire_ckbx.getText());
                values_to_enter_panel.add(dire_panel);
            }
        }
        this.updateUI();
    }
    
    /**
     * 
     * @param e 
     * @author arthur
     * source https://docs.oracle.com/javase/tutorial/uiswing/components/button.html
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==search_rbut){
            //System.out.println("search");
            input_panel.remove(add_radio_buttons_panel);
            input_panel.remove(patient_values_to_enter_panel);
            input_panel.remove(doctor_values_to_enter_panel);
            input_panel.remove(nurse_values_to_enter_panel);
            input_panel.remove(values_to_enter_panel);
            input_panel.add(checkbox_panel_2);
            input_panel.add(type_and_condition_panel);
            
            this.updateUI();
        }
        else if(e.getSource()==add_rbut)
        {
            //System.out.println("add");
            input_panel.remove(checkbox_panel_2);
            input_panel.remove(values_to_enter_panel);
            input_panel.remove(type_and_condition_panel);
            input_panel.add(add_radio_buttons_panel);
            input_panel.add(patient_values_to_enter_panel);
            
            this.updateUI();
        }
        else if(e.getSource()==update_rbut)
        {
            //System.out.println("Update");
            input_panel.remove(add_radio_buttons_panel);
            input_panel.remove(patient_values_to_enter_panel);
            input_panel.remove(doctor_values_to_enter_panel);
            input_panel.remove(nurse_values_to_enter_panel);
            input_panel.add(checkbox_panel_2);
            input_panel.add(values_to_enter_panel);
            input_panel.add(type_and_condition_panel);
            
            
            this.updateUI();
        }
        else if(e.getSource()==patient_rbut){
            input_panel.remove(doctor_values_to_enter_panel);
            input_panel.remove(nurse_values_to_enter_panel);
            patient_values_to_enter_panel.add(fpat_panel);
            patient_values_to_enter_panel.add(lpat_panel);
            patient_values_to_enter_panel.add(apat_panel);
            patient_values_to_enter_panel.add(ppat_panel);
            patient_values_to_enter_panel.add(mutu_panel);
            patient_values_to_enter_panel.add(nemp_panel);
            patient_values_to_enter_panel.add(nroo_panel);
            patient_values_to_enter_panel.add(nbed_panel);
            patient_values_to_enter_panel.add(serv_panel);
            input_panel.add(patient_values_to_enter_panel);
            this.updateUI();
        }
        else if(e.getSource()==doctor_rbut){
            input_panel.remove(patient_values_to_enter_panel);
            input_panel.remove(nurse_values_to_enter_panel);
            doctor_values_to_enter_panel.add(lemp_panel);
            doctor_values_to_enter_panel.add(femp_panel);
            doctor_values_to_enter_panel.add(aemp_panel);
            doctor_values_to_enter_panel.add(pemp_panel);
            doctor_values_to_enter_panel.add(spec_panel);
            input_panel.add(doctor_values_to_enter_panel);
            this.updateUI();
        }
        else if(e.getSource()==nurse_rbut){
            input_panel.remove(doctor_values_to_enter_panel);
            input_panel.remove(patient_values_to_enter_panel);
            nurse_values_to_enter_panel.add(lemp_panel);
            nurse_values_to_enter_panel.add(femp_panel);
            nurse_values_to_enter_panel.add(aemp_panel);
            nurse_values_to_enter_panel.add(pemp_panel);
            nurse_values_to_enter_panel.add(sala_panel);
            nurse_values_to_enter_panel.add(rota_panel);
            nurse_values_to_enter_panel.add(serv_panel);
            input_panel.add(nurse_values_to_enter_panel);
            this.updateUI();
        }
        else if(e.getSource()== stats_button){
            CardLayout cl;
            cl = (CardLayout)(cards.getLayout());
            cl.next(cards);
        }
        else if(e.getSource()== r_button){
            CardLayout cl;
            cl = (CardLayout)(cards.getLayout());
            cl.last(cards);
        }
        else if(e.getSource()== done_button){
            //launch search, add or update
            if(search_rbut.isSelected()){
                statement_type = Type_Query.SELECT;
                
                //Fields
                if(serv_ckbx.isSelected()){
                    fields.add("code_service");
                }
                if(nroo_ckbx.isSelected()){
                    fields.add("no_chambre");
                }
                if(nemp_ckbx.isSelected()){
                    fields.add("no_employe");
                }
                if(beds_ckbx.isSelected()){
                    fields.add("nb_lits");
                }
                if(spec_ckbx.isSelected()){
                    fields.add("specialite");
                }
                if(lemp_ckbx.isSelected()){
                    fields.add("nom_employe");
                }
                if(femp_ckbx.isSelected()){
                    fields.add("prenom_employe");
                }
                if(aemp_ckbx.isSelected()){
                    fields.add("adresse_employe");
                }
                if(pemp_ckbx.isSelected()){
                    fields.add("tel_employe");
                }
                if(npat_ckbx.isSelected()){
                    fields.add("no_malade");
                }
                if(nbed_ckbx.isSelected()){
                    fields.add("no_lit");
                }
                if(rota_ckbx.isSelected()){
                    fields.add("rotation");
                }
                if(sala_ckbx.isSelected()){
                    fields.add("salaire");
                }
                if(lpat_ckbx.isSelected()){
                    fields.add("nom_malade");
                }
                if(fpat_ckbx.isSelected()){
                    fields.add("prenom_malade");
                }
                if(apat_ckbx.isSelected()){
                    fields.add("adresse_malade");
                }
                if(ppat_ckbx.isSelected()){
                    fields.add("tel_malade");
                }
                if(mutu_ckbx.isSelected()){
                    fields.add("mutuelle");
                }
                if(nser_ckbx.isSelected()){
                    fields.add("nom_service");
                }
                if(buil_ckbx.isSelected()){
                    fields.add("batiment");
                }
                if(dire_ckbx.isSelected()){
                    fields.add("no_employe");
                }
                
                //Tables
                if(soigne_ckbx.isSelected()){
                    tables.add("soigne");
                }
                if(chambre_ckbx.isSelected()){
                    tables.add("chambre");
                }
                if(hospitalisation_ckbx.isSelected()){
                    if(chambre_ckbx.isSelected()){
                        tables.add("chambre");
                    }
                    else tables.add("hospitalisation");
                }
                if(docteur_ckbx.isSelected()){
                    tables.add("docteur");
                }
                if(employe_ckbx.isSelected()){
                    tables.add("employe");
                }
                if(infirmier_ckbx.isSelected()){
                    tables.add("infirmier");
                }
                if(malade_ckbx.isSelected()){
                    tables.add("malade");
                }
                if(service_ckbx.isSelected()){
                    tables.add("service");
                }
                //System.out.println(tables);
                
                //Against ambiguious 
                ArrayList<String> al1 = new ArrayList<String>();
                
                //no_employe
                if(nemp_ckbx.isSelected()){
                    if(tables.contains("employe")) al1.add("employe");
                    if(tables.contains("docteur")) al1.add("docteur");
                    if(tables.contains("soigne")) al1.add("soigne");
                    if(tables.contains("chambre")) al1.add("chambre");
                    
                    if(al1.size() >= 2){
                        /*int index;
                        for(int i = 0; i < fields.size(); i++){
                            if(fields.get(i).equals("no_employe")) index = i;
                        }*/
                        fields.remove("no_employe");
                        fields.add(al1.get(0) + ".no_employe");
                    }
                }
                
                al1.clear();       
                //no_malade
                if(npat_ckbx.isSelected()){
                    if(tables.contains("malade")) al1.add("malade");
                    if(tables.contains("hospitalisation")) al1.add("hospitalisation");
                    if(tables.contains("soigne")) al1.add("soigne");
                    
                    if(al1.size() >= 2){
                        fields.remove("no_malade");
                        fields.add(al1.get(0) + ".no_malade");
                    }
                }
                
                al1.clear();
                //code_service
                if(serv_ckbx.isSelected()){
                    if(tables.contains("chambre")) al1.add("chambre");
                    if(tables.contains("hospitalisation")) al1.add("hospitalisation");
                    if(tables.contains("infirmier")) al1.add("infirmier");
                    if(tables.contains("service")) al1.add("service");
                    
                    if(al1.size() >= 2){
                        fields.remove("code_service");
                        fields.add(al1.get(0) + ".code_service");
                    }
                }
                
                al1.clear();
                //no_chambre
                if(nroo_ckbx.isSelected()){
                    if(tables.contains("chambre")) al1.add("chambre");
                    if(tables.contains("hospitalisation")) al1.add("hospitalisation");
                    
                    if(al1.size() >= 2){
                        fields.remove("no_chambre");
                        fields.add(al1.get(0) + ".no_chambre");
                    }
                }
            }
            else if(add_rbut.isSelected()){
                /* Tentatives de créer un insert
                statement_type = Type_Query.INSERT;
                
                if(nurse_rbut.isSelected()){
                    tables.add("infirmier");
                    //fields.add("no_employe");
                    fields.add("salaire");
                    fields.add("code_service");
                    fields.add("rotation");
                    //set.add("17");
                    set.add(sala_txt.getText());
                    set.add(serv_txt.getText());
                    set.add(rota_txt.getText());
                    new Creation(statement_type, fields, tables, set,null, null, null);
                    tables.clear();
                    fields.clear();
                    set.clear();
                    tables.add("employe");
                    fields.add("nom_employe");
                    fields.add("prenom_employe");
                    fields.add("adresse_employe");
                    fields.add("tel_employe");
                    set.add(lemp_txt.getText());
                    set.add(femp_txt.getText());
                    set.add(aemp_txt.getText());
                    set.add(ppat_txt.getText());
                    new Creation(statement_type, fields, tables, set,null, null, null);
                }else if(doctor_rbut.isSelected()){
                    new Creation(statement_type, fields, tables, set,null, null, null);
                    new Creation(statement_type, fields, tables, set,null, null, null);
                }else if(patient_rbut.isSelected()){
          
                }
                */
            }
            else if(update_rbut.isSelected()){
                statement_type = Type_Query.UPDATE;
            }
            
            //Call to Creation class
            results_panel.removeAll();
            
            //Condition preparation
            Condition cond = null;
            if(type_cbx.getSelectedItem() != null && txt_to_srch.getText() != null && !txt_to_srch.getText().equals("Precise your search here")){
                String condition_table;
                String condition_field;
                String condition_match;
                
                condition_match = "'%";
                condition_match += txt_to_srch.getText();
                condition_match += "%'";
                
                switch((String) type_cbx.getSelectedItem()){
                    case "Service code" :
                        if(infirmier_ckbx.isSelected()){
                            condition_field = "infirmier.code_service";
                            condition_table = "infirmier";
                        }
                        else if(serv_ckbx.isSelected()){
                            condition_field = "service.code_service";
                            condition_table = "service";
                        }
                        else if(chambre_ckbx.isSelected()){
                            condition_field = "chambre.code_service";
                            condition_table = "chambre";
                        }
                        else{
                            condition_field = "hospitalisation.code_service";
                            condition_table = "hospitalisation";
                        }
                        break;
                    case "Room n°" :
                        if(chambre_ckbx.isSelected()){
                            condition_field = "chambre.no_chambre";
                            condition_table = "chambre";
                        }  
                        else{
                            condition_field = "hospitalisation.no_chambre";
                            condition_table = "hospitalisation";
                        }
                        break;
                    case "Employe n°" :
                        if(employe_ckbx.isSelected()){
                            condition_field = "employe.no_employe";
                            condition_table = "employe";
                        }
                        else if(docteur_ckbx.isSelected()){
                            condition_field = "docteur.no_employe";
                            condition_table = "docteur";
                        }
                        else if(infirmier_ckbx.isSelected()){
                            condition_field = "infirmier.no_employe";
                            condition_table = "infirmier";
                            } 
                        else if(chambre_ckbx.isSelected()){
                            condition_field = "chambre.no_employe";
                            condition_table = "chambre";
                            }
                        else{
                            condition_field = "soigne.no_employe";
                            condition_table = "soigne";
                        }
                        break;
                    case "Number of beds" : condition_table = "chambre";
                        condition_field = "nb_lits";
                        break;
                    case "Specialty" : condition_table = "docteur";
                        condition_field = "specialite";
                        break;
                    case "Employee last name" : condition_table = "employe";
                        condition_field = "nom_employe";
                        break;
                    case "Employee first name" : condition_table = "employe";
                        condition_field = "prenom_employe";
                        break;
                    case "Employee address" : condition_table = "employe";
                        condition_field = "adresse_employe";
                        break;
                    case "Employee phone n°" : condition_table = "employe";
                        condition_field = "tel_employe";
                        break;
                    case "Patient n°" :
                        if(malade_ckbx.isSelected()){
                            condition_field = "malade.no_malade";
                            condition_table = "malade";
                        }
                        else if(hospitalisation_ckbx.isSelected()){
                            condition_field = "hospitalisation.no_malade";
                            condition_table = "hospitalisation";
                        }
                        else{
                            condition_field = "soigne.no_malade";
                            condition_table = "soigne";
                        }
                        break;
                    case "Bed n°" : condition_table = "hospitalisation";
                        condition_field = "no_lit";
                        break;
                    case "Rotation" : condition_table = "infirmier";
                        condition_field = "rotation";
                        break;
                    case "Salary" : condition_table = "infirmier";
                        condition_field = "salaire";
                        break;
                    case "Patient last name" : condition_table = "malade";
                        condition_field = "nom_malade";
                        break;
                    case "Patient first name" : condition_table = "malade";
                        condition_field = "prenom_malade";
                        break;
                    case "Patient address" : condition_table = "malade";
                        condition_field = "adresse_malade";
                        break;
                    case "Patient phone n°" : condition_table = "malade";
                        condition_field = "tel_malade";
                        break;
                    case "Mutual" : condition_table = "malade";
                        condition_field = "mutuelle";
                        break;
                    case "Building" : condition_table = "service";
                        condition_field = "batiment";
                        break;
                    case "Director" : condition_table = "service";
                        condition_field = "directeur";
                        break;
                    default : condition_table = " ";
                        condition_field = " ";
                        break;
                }
                System.out.println(condition_table);
                cond = new Condition(condition_table, condition_field, condition_match);
            }
            
            Creation query;
            switch(statement_type){
                case SELECT : 
                    query = new Creation(statement_type, fields, tables, null, (cond != null) ? cond : null, null, null);
                    raw_result = query.data();
                    break;
                /*
                case INSERT :
                    query = new Creation(statement_type, fields, tables, values, null, null, null);
                    break;
                */
                case UPDATE :
                    query = new Creation(statement_type, fields, tables, values, (cond != null) ? cond : null, null, null);
                    break;
            }

            System.out.print(fields);
            results_panel.removeAll();
            tables.clear();

            res_card = new Results_card(raw_result, fields);
            res_card.init();
            results_panel.add(res_card.getGlobal());
            
            main_panel.revalidate();
            main_panel.repaint();
            
            fields.clear();
            tables.clear();
            txt_to_srch.setText("Precise your search here");
        }
    }
    
    @Override
    public void focusGained(FocusEvent e){
        txt_to_srch.setText("");
    }
    @Override
    public void focusLost(FocusEvent e){
        
    }
    public void update_screen(){
        main_panel.add(input_panel);
        main_panel.add(results_panel);
        this.add(main_panel);
    }
    
    public static void insertIntoCombo(JComboBox combo,Object item){
        if (item == null) {
          return;
        }
        MutableComboBoxModel model=(MutableComboBoxModel)combo.getModel();
        if (model.getSize() == 0) {
          model.insertElementAt(item,0);
          return;
        }
        Object o=model.getElementAt(0);
        if (o.equals(item)) {
          return;
        }
        model.removeElement(item);
        model.insertElementAt(item,0);
        combo.setSelectedIndex(0);
      }
}
