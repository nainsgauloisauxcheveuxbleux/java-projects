 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.CardLayout;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Main view of the interface for the project
 * 
 * @author arthur
 * @version 2.0
 * @since 21/04
 */
public class GUI extends JFrame /*implements ActionListener*/{
    
    private final JPanel cards = new JPanel(new CardLayout());
    private final Main_card main_card = new Main_card(cards);
    private final Stats_card stats_card = new Stats_card(cards);
    private final R_card r_card = new R_card(cards);
    
    /**
     * Constructor
     * 
     */
    public GUI(){
        super();
    }
    
    /**
     * GUI use a system of cards, init() creates and uses them and there relations
     * 
     * @author arthur
     */
    public void init(){
        //Setting the title
        this.setTitle("Hospital manager");
        //Setting the size
        this.setSize(1080,720);
        //this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        //Center object
        this.setLocationRelativeTo(null);
        //Exit behaviour
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
        try {
            this.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("test.jpg")))));
        } catch (IOException e) {
        }
        */
        //Main card
        main_card.init();
        main_card.setName("main_card");
        this.cards.add(main_card);
        
        //Stat card
        stats_card.init();
        stats_card.setName("stats_card");
        this.cards.add(stats_card);

        //R card
        r_card.init();
        r_card.setName("r_card");
        this.cards.add(r_card);
        
        this.add(cards);
        this.setVisible(true);
    }
}
