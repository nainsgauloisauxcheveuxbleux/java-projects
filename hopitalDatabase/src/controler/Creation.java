/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import model.Search;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcv2018.Connexion;
import model.Result_line;

/**
 * 3 phases : 
 * - create the sql request
 * - call the bdd 
 * - retrieve the result, treat it and return the final object to the Swing interface
 * 
 * 
 * @author Gabriel
 */
public class Creation {
    
    //PREPARE
    //Variables
    ArrayList<String> columns;
    ArrayList<String> tables;
    ArrayList<String> values;
    Condition condition;
    Type_Query type;
    String group_by;
    String order_by;
    
    String SQL_query = "";
   
    
    Search Search_result;
    
    
    //Constructor
    
    /**
     * Constructor sql statement
     * 
     * @param _type define SELECT, INSERT or UPDATE
     * @param _columns AL of the wanted columns
     * @param _tables AL of the tables the columns are from
     * @param _values AL of the values to INSERT or UPDATE
     * @param _condition WHERE condition
     * @param _group_by 
     * @param _order_by
     * 
     * @author Gabriel
     * @version 1.2
     * @since 19/04
     */
    public Creation(Type_Query _type, ArrayList<String> _columns, ArrayList<String> _tables, ArrayList<String> _values, Condition _condition, String _group_by, String _order_by){ 
        /*
        columns are the wanted columns (so in SELECT)
        tables are the tables they are from (do inner join in FROM)
        Condition is class
        
        @author Gabriel
        */
        if((_columns == null) || (_tables == null)){
            System.out.println("One of the given parameters is Empty");
        }
        else {
            columns = _columns;
            tables = _tables;
            values = _values;
            condition  = _condition;
            type = _type;
            group_by = _group_by;
            order_by = _order_by;
        
            prepare();
            run();
        }
    }
    
    public Creation(String sql){
        SQL_query = sql;
        type = Type_Query.SELECT;
        //System.out.println(SQL_query);
        run();
    }
    
    //methods
    /**
     * Prepare the sql statement given the type and the given parameter
     * 
     * @author Gabriel
     * @version 1.1
     * @since 16/04
     */
    private void prepare(){
        //switch to deferentiate SELECT, INSERT INTO, UPDATE
        switch(type){
            case SELECT :
                String column_innerjoin = "";
        
                //SELECT
                SQL_query = "SELECT ";
                for(int  i = 0; i < columns.size(); i ++){
                    if(i != columns.size() - 1){
                        SQL_query += columns.get(i) + ", ";
                    }
                    else {
                        SQL_query += columns.get(i); //so don"t have a last comma
                    }
                }

                //FROM
                /*If I have more than one table then i need to inner join the ones that are given to me.
                In order to do that I have to get all the columns from the said tables and see the one they have in common and match those
                */
                SQL_query += " FROM ";
                if(tables.size() == 1){
                    SQL_query += tables.get(0);
                }
                else if(tables.size() == 2){
                    //case for 2 tables
                    HashMap map = new HashMap();

                    try {
                         Connexion  local = new Connexion("hopital", "root", ""); //connect to the local database

                        for(int i = 0; i < 2; i++)
                        {
                            ArrayList<ArrayList<String>> global_AL = null;
                            try {

                                //get the array with the columns name
                                global_AL = local.remplirChampsTable("SELECT * FROM " + tables.get(i));
                                ArrayList<String> column_name = global_AL.get(0);

                                for(String name : column_name){
                                    //System.out.println("nom colonne :" + name);
                                    boolean is_in_map = map.containsKey(name);
                                    if(is_in_map == true){
                                        //there is already this column name in the map
                                        //so it's a join possibility
                                        column_innerjoin = name;
                                    }
                                    else {
                                        //it's not in the map yet
                                        map.put(name, name);
                                    }
                                }

                            } catch (SQLException ex) {
                                Logger.getLogger(QuickSearch.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    } catch (SQLException ex) {
                        Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //System.out.print("column innerjoin : " + column_innerjoin);
                    //System.out.print("QUERY : " + SQL_query);
                    
                    SQL_query += tables.get(0) + " INNER JOIN " + tables.get(1) + " ON ";
                    SQL_query += tables.get(0) + "." + column_innerjoin + " = " + tables.get(1) + "." + column_innerjoin;
                }
                else { //for n tables :
                    //sql format : SELECT * FROM soigne INNER JOIN hospitalisation ON soigne.no_malade = hospitalisation.no_malade INNER JOIN employe ON soigne.no_employe = employe.no_employe INNER JOIN malade ON soigne.no_malade = malade.no_malade

                    HashMap name_map = new HashMap();
                    ArrayList<NameFrom> match = new ArrayList();

                    try {
                        Connexion  local = new Connexion("hopital", "root", ""); //connect to the local database

                        for(int i = 0; i < tables.size(); i++)
                        {
                            ArrayList<ArrayList<String>> global_AL = null;
                            try {

                                //get the array with the columns name
                                global_AL = local.remplirChampsTable("SELECT * FROM " + tables.get(i));
                                ArrayList<String> column_name = global_AL.get(0);

                                for(String name : column_name){
                                    //System.out.println("nom colonne :" + name);
                                    boolean is_in_map = name_map.containsKey(name);
                                    if(is_in_map == true){
                                        //there is already this column name in the map
                                        //so it's a join possibility
                                        // add to the column name and the tables it's joining
                                        // the same name can be joining 2 tables !!!!
                                        ArrayList<String> temp = (ArrayList<String>) name_map.get(name);

                                        //add the column name where to join, the name of the current table and the name of the recorded table for the last occurence of name in the hashmap
                                        NameFrom connection = new NameFrom(name, tables.get(i), temp.get(1));
                                        match.add(connection);

                                    }
                                    else {
                                        //it's not in the map yet
                                        ArrayList<String> name_table = new ArrayList();
                                        name_table.add(name);
                                        name_table.add(tables.get(i));

                                        //add at index name an array containing the name of the column and the table it's from
                                        name_map.put(name, name_table);
                                    }
                                }

                            } catch (SQLException ex) {
                                Logger.getLogger(QuickSearch.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }


                    } catch (SQLException ex) {
                        Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    
                    //match the inner join and on on the columns and tables
                    int count = 0;
                    for(NameFrom elem : match){
                        //System.out.println("elem table " + elem.table_innerjoin_name.get(0) + " , " + elem.table_innerjoin_name.get(1));
                        if( count >= 1){
                            SQL_query += "INNER JOIN " + elem.table_innerjoin_name.get(0) + " ON " + elem.table_innerjoin_name.get(0) + "." + elem.column_innerjoin_name + " = " + elem.table_innerjoin_name.get(1) + "." + elem.column_innerjoin_name + " ";
                            count++;
                        }
                        else {
                            SQL_query += elem.table_innerjoin_name.get(0) + " INNER JOIN " + elem.table_innerjoin_name.get(1) + " ON " + elem.table_innerjoin_name.get(0) + "." + elem.column_innerjoin_name + " = " + elem.table_innerjoin_name.get(1) + "." + elem.column_innerjoin_name + " ";
                            count++;
                        }
                        
                    }
                }

                //WHERE
                if(condition != null){
                    SQL_query += " WHERE ";
                    SQL_query += condition.table + "." + condition.field + " LIKE " + condition.s_to_match;
                }
                
                //GROUP BY
                if(group_by != null){
                    SQL_query += " GROUP BY ";
                    SQL_query += group_by;
                }
                
                //ORDER BY
                if(order_by != null){
                    SQL_query += " ORDER BY ";
                    SQL_query += order_by;
                }
                
                break;
            
            case INSERT :
                //INSERT INTO
                SQL_query = "INSERT INTO ";
                SQL_query += tables.get(0);
                SQL_query += " (";
                for(int  i = 0; i < columns.size(); i ++){
                    if(i != columns.size() - 1){
                        SQL_query += columns.get(i) + ", ";
                    }
                    else {
                        SQL_query += columns.get(i); //so don"t have a last comma
                    }
                }
                SQL_query += " )";
                
                //VALUES
                SQL_query += " VALUES ";
                SQL_query += " (";
                for(int  i = 0; i < values.size(); i ++){
                    if(i != values.size() - 1){
                        SQL_query += values.get(i) + ", ";
                    }
                    else {
                        SQL_query += values.get(i); //so don"t have a last comma
                    }
                }
                SQL_query += " )";
                
                break;
            
            case UPDATE :
                //UPDATE
                SQL_query = "UPDATE ";
                SQL_query += tables.get(0);
                
                //SET
                SQL_query += " SET ";
                for(int  i = 0; i < columns.size(); i ++){
                    if(i != columns.size() - 1){
                        SQL_query += columns.get(i) + " = " + values.get(i) + ", ";
                    }
                    else {
                        SQL_query += columns.get(i) + " = " + values.get(i); //so don"t have a last comma
                    }
                }
                
                //WHERE
                if(condition != null){
                    SQL_query += " WHERE ";
                    SQL_query += condition.table + "." + condition.field + " LIKE " + condition.s_to_match;
                }
                break;
        }
        
        //Verification
        System.out.println("Creation : " + SQL_query);
    }
    
    /**
     * Run the corresponding sql statement to the correct function of jdbc
     * 
     * @source : https://www.tutorialspoint.com/jdbc/jdbc-insert-records.htm
     * @author Gabriel
     * @version 1.0 
     */
    private void run(){
        try {
            
            Connexion local = new Connexion("hopital", "root", "");
            
            switch(type){
                case SELECT :
                    Search_result = new Search((ArrayList<String>) local.remplirChampsTable(SQL_query).get(0), local.remplirChampsRequete(SQL_query));
                    break;
                
                case INSERT :
                    local.executeUpdate(SQL_query);
                    break;
                    
                case UPDATE : 
                    local.executeUpdate(SQL_query);
                    break;
                default:
                    throw new AssertionError(type.name());
            }

        } catch (SQLException ex) {
            Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Creation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /***
     * Public function to use after the object was created to retrieve all the data correctly formated
     * @return AL of Result_line
     * @author Henri, Gabriel
     */
    public ArrayList<Result_line> data(){
        return Search_result.extract_result();
    }
   
}
