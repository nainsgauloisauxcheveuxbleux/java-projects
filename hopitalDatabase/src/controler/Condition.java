/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

/**
 * Condition class for Where clause in sql query
 * 
 * @author Gabriel
 */
public class Condition {
    public String table;
    public String field;
    public String s_to_match;
    public int i_to_match;
    
    public Condition(String _table, String _field, String _s_to_match){
        table = _table;
        field = _field;
        s_to_match = _s_to_match;
    }
}
