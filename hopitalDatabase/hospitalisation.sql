-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2018 at 08:43 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hopital`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospitalisation`
--

CREATE TABLE `hospitalisation` (
  `no_malade` decimal(4,0) NOT NULL,
  `code_service` char(3) NOT NULL,
  `no_chambre` decimal(3,0) NOT NULL,
  `no_lit` decimal(2,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospitalisation`
--

INSERT INTO `hospitalisation` (`no_malade`, `code_service`, `no_chambre`, `no_lit`) VALUES
('1', 'REA', '101', '1'),
('3', 'REA', '102', '1'),
('6', 'REA', '103', '1'),
('21', 'REA', '103', '2'),
('33', 'REA', '104', '1'),
('36', 'REA', '104', '2'),
('37', 'CHG', '201', '1'),
('41', 'CHG', '201', '2'),
('43', 'CHG', '201', '3'),
('46', 'CHG', '202', '2'),
('52', 'CHG', '202', '3'),
('55', 'CHG', '202', '4'),
('56', 'CHG', '301', '1'),
('61', 'CHG', '301', '2'),
('65', 'CHG', '302', '1'),
('66', 'CHG', '302', '2'),
('67', 'CHG', '303', '1'),
('68', 'CAR', '101', '1'),
('72', 'CAR', '101', '3'),
('74', 'CAR', '102', '1'),
('76', 'CAR', '102', '2'),
('77', 'CAR', '103', '1'),
('103', 'REA', '105', '1'),
('105', 'REA', '107', '1'),
('108', 'REA', '107', '2'),
('117', 'REA', '108', '1'),
('120', 'CHG', '401', '1'),
('123', 'CHG', '401', '4'),
('137', 'CHG', '402', '1'),
('145', 'CHG', '402', '2'),
('147', 'CHG', '402', '3'),
('149', 'CHG', '403', '1'),
('154', 'CHG', '403', '2'),
('159', 'CHG', '404', '2'),
('167', 'CHG', '405', '1'),
('172', 'CAR', '104', '1'),
('182', 'CAR', '104', '3'),
('188', 'CAR', '105', '2'),
('192', 'CAR', '106', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospitalisation`
--
ALTER TABLE `hospitalisation`
  ADD PRIMARY KEY (`no_malade`),
  ADD UNIQUE KEY `code_service` (`code_service`,`no_chambre`,`no_lit`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
