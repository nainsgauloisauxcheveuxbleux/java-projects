package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import reporting.ElementsReport;

/**
 * Print of the chart corresponding to the one we want
 * 
 * @author Gabriel, Henri
 */
public class Stats_card extends JPanel {
    private final JPanel cards;
    private JLabel text = new JLabel("Reporting : ");
    
    private JPanel global = new JPanel();//A mettre dans this
    private JPanel button_container = new JPanel();//Contient les boutons, à mettre dans global, north
    private JPanel chart = new JPanel();//Contient Chart à mettre dans global, center
    private JPanel Stats = this;
    private ElementsReport data = new ElementsReport();
    
    private ChartPanel Doctor_repartition_panel = data.getDoctor_repartition_panel();
    private ChartPanel Mutual_repartition_panel = data.getMutual_repartition_panel();
    private ChartPanel Salary_service_repartition_panel = data.getSalary_service_repartition_panel();
    private ChartPanel Ratio_nurse_per_patient_panel = data.getRatio_nurse_per_patient_panel();
    private ChartPanel Ratio_bed_per_room_repartition = data.getRatio_bed_per_room_panel();
    
    private JButton return_button;
    
    /**
     * Constructor
     */
    public Stats_card(JPanel _cards){

        cards =_cards;
    }
    
    /**
     * Add the button and handle the interaction with the user 
     * Draw the wanted chart
     * 
     */
    public void init(){
        
        button_container.setLayout(new GridLayout(1,4));
        global.setLayout(new BorderLayout());
        this.setLayout(new BorderLayout());
        
        JButton chart1 = new JButton("Repartition of doctors");
        chart1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //Action of chart 2
                chart.removeAll();
                chart.add(Doctor_repartition_panel,BorderLayout.CENTER);
                Stats.revalidate();
                Stats.repaint();
            }
        });
        
        JButton chart2 = new JButton("Repartition of mutual");
        chart2.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //Action of chart 2
                chart.removeAll();
                chart.add(Mutual_repartition_panel,BorderLayout.CENTER);
                Stats.revalidate();
                Stats.repaint();
            }
        });
        
        JButton chart3 = new JButton("Ratio of bed per room");
        chart3.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //Action of chart 3
                chart.removeAll();
                chart.add(Ratio_bed_per_room_repartition,BorderLayout.CENTER);
                Stats.revalidate();
                Stats.repaint();
            }
        });
        
        JButton chart4 = new JButton("Average nurse salary");
        chart4.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //Action of chart 4
                chart.removeAll();
                chart.add(Salary_service_repartition_panel,BorderLayout.CENTER);
                Stats.revalidate();
                Stats.repaint();
            }
        });
        
        JButton chart5 = new JButton("Ratio Nurse Patient");
        chart5.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                //Action of chart 5
                chart.removeAll();
                chart.add(Ratio_nurse_per_patient_panel,BorderLayout.CENTER);
                Stats.revalidate();
                Stats.repaint();
            }
        });
        
        
        return_button = new JButton("Return to main menu");
        return_button.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                CardLayout cl;
                cl = (CardLayout)(cards.getLayout());
                cl.first(cards);
            }
        });
        
        //Defining which panels contain other panels
        button_container.add(chart1);
        button_container.add(chart2);
        button_container.add(chart3);
        button_container.add(chart4);
        button_container.add(chart5);
        
        global.add(button_container,BorderLayout.NORTH);
        global.add(chart,BorderLayout.CENTER);
        global.add(return_button,BorderLayout.SOUTH);
        
        //Adding the buffer global to the main Panel and making it visible
        this.add(global);
        this.setVisible(true);
    }
    /**
     * Getter to return to main menu
     * 
     * @return 
     */
    public JButton getReturn_button(){
        return return_button;
    }

}
