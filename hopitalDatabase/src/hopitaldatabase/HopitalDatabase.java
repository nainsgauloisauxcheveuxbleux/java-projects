/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hopitaldatabase;

import GUI.*;
import java.sql.SQLException;
import jdbcv2018.Connexion;

 /**
 * Main file
 * 
 * @author Gabriel, Henri, Arthur
 */
public class HopitalDatabase {

    /**
     * 
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        // TODO code application logic here
        
        //local
        Connexion local = new Connexion("hopital", "root", "");
        
        /**
         * Connexion to the DB succesfull in local
         * 
         */
        /*
        System.out.println("Affiche toutes les lignes du tableau : " + local.remplirChampsTable("SELECT * FROM chambre"));
        
        ArrayList<ArrayList> global_AL = local.remplirChampsTable("SELECT * FROM chambre");
        ArrayList<String> line = global_AL.get(0);
        System.out.println("Affihce un des elements du tableau : " + line.get(2));
        
        
        ArrayList<ArrayList> global_chambre = local.remplirChampsRequete("SELECT * FROM chambre");
        for(ArrayList<String> chambre : global_chambre){
            for(String str : chambre){
                System.out.print("\t" + str);
            }
            System.out.println("");            
        }
        */
        
        /**
         * Connexion to ece's db to test (server were overloaded)
         * 
         */
        /*
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        String pswd = sc.nextLine();
        //distant
        Connexion distant = new Connexion(login,pswd,"gp152055-rw","yX8WAHgz");
        System.out.println(distant.remplirChampsTable("docteur"));
        ArrayList<String> docteur = distant.remplirChampsRequete("select * from docteur");
        for(String obj : docteur){
            System.out.print((String) obj);
        }
        */
        
        /**
         * Test 1 of Creation.java and every classes it uses (a lot of stuff)
         */
        /******* LIGNE A RETIRER POUR CODER 
        ArrayList colums = new ArrayList();
        colums.add("employe.nom_employe, employe.prenom_employe, infirmier.code_service, infirmier.salaire, service.nom_service"); //have to precise from which table yoou want it
        ArrayList tables = new ArrayList();
        tables.add("infirmier");
        tables.add("service");
        tables.add("employe");
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, null);
        
        //Just to make sure everything before went all according to KEIKAKU
        System.out.println("Creation Class constructor working just fine");
        
        ArrayList<Result_line> usable = Test_request.data();
        for(int i =0; i< usable.size(); i++){
            System.out.println(usable.get(i).getDepartment_code());
            System.out.println(usable.get(i).getDepartment_name());
            System.out.println(usable.get(i).getSalary());
            System.out.println(usable.get(i).getPatient_firstname());
            System.out.println(usable.get(i).getPatient_name());
        }
        */
        /**
         * End of test 1
         */
        
        
        /**
         * Test 2
         */
        /*
        ArrayList colums = new ArrayList();
        colums.add("employe.nom_employe, malade.nom_malade, malade.prenom_malade, malade.tel_malade, malade.mutuelle"); //have to precise from which table you want it
        ArrayList tables = new ArrayList();
        tables.add("employe");
        tables.add("soigne");
        tables.add("malade");
        Condition cond = new Condition("malade", "mutuelle", "\'%MN%\'");
        */
        
        /**
         * Test 3
         */
        /*
        ArrayList colums = new ArrayList();
        colums.add("employe.nom_employe, employe.prenom_employe, infirmier.code_service, infirmier.salaire, service.nom_service");
        ArrayList tables = new ArrayList();
        tables.add("infirmier");
        tables.add("service");
        tables.add("employe");
        */
        /*
        //Select method with columns, tables, no values to insert, a where condition, no group or order by
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, cond, null, null);
        
        //Just to make sure everything before went all according to KEIKAKU
        System.out.println("Creation Class constructor working just fine");
        
        ArrayList<Result_line> usable = Test_request.data();
        for(int i =0; i< usable.size(); i++){
            System.out.println(usable.get(i).getEmployee_name());
            System.out.println(usable.get(i).getPatient_name());
            System.out.println(usable.get(i).getPatient_firstname());
            System.out.println(usable.get(i).getPatient_phone_number());
            System.out.println(usable.get(i).getMutual_health());
        }
        */
        
        //ElementsReport Reporting = new ElementsReport();
        
        GUI guy = new GUI();
        guy.init();
    }    
}
