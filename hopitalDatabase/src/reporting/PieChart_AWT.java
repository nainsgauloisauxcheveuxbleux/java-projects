/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting;

import java.awt.Color;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author Henri
 * source : https://www.tutorialspoint.com/jfreechart/jfreechart_pie_chart.htm
 */
public class PieChart_AWT extends ApplicationFrame{
    
    String title;
    ArrayList<ArrayList> Chart_data;
    ChartPanel chartPanel;
    
   public PieChart_AWT(String title, ArrayList<ArrayList> _Chart_data){
      super(title);
      Chart_data = _Chart_data;
      PieDataset dataset = createDataset();
      JFreeChart pie_chart = ChartFactory.createPieChart(
         title,
         dataset,
         true,// whether the chart includes legend or not
         true,
         false);
      
      chartPanel = new ChartPanel(pie_chart);
   }
      
   private PieDataset createDataset() {
      DefaultPieDataset dataset = new DefaultPieDataset();
      for(int i=0; i < Chart_data.size(); i++){
          dataset.setValue((String)Chart_data.get(i).get(1),(int)Chart_data.get(i).get(0));
      }
      return dataset;
   }

    public ChartPanel getChartPanel() {
        return chartPanel;
    }
   
}



