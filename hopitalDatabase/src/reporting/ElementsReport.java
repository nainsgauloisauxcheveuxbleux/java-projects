/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting;

import controler.Creation;
import controler.QuickSearch;
import controler.Type_Query;
import java.awt.Color;
import java.util.ArrayList;
import model.Result_line;
import org.jfree.data.general.DefaultPieDataset;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;


/**
 * Class of reporting examples
 * 
 * @author Gabriel
 */
public class ElementsReport{
    
    //pie charts data
    ArrayList<ArrayList> doctor_repartition;
    ArrayList<ArrayList> mutual_repartition;
    
    //bar charts data
    ArrayList<ArrayList> salary_service_repartition;
    ArrayList<ArrayList> ratio_nurse_per_patient;
    ArrayList<ArrayList> ratio_bed_per_room;
    
    //Pie charts objetcs
    PieChart_AWT Doctor_repartition_chart;
    PieChart_AWT Mutual_repartition_chart;
    
    //Bar charts objects
    BarChart_AWT Salary_service_repartition_chart;
    BarChart_AWT Ratio_nurse_per_patient_chart;
    BarChart_AWT ratio_bed_per_room_chart;
    
    //JPanels linked to each chart
    ChartPanel Doctor_repartition_panel;
    ChartPanel Mutual_repartition_panel;
    ChartPanel Salary_service_repartition_panel;
    ChartPanel Ratio_nurse_per_patient_panel;
    ChartPanel Ratio_bed_per_room_panel;
    
    public ElementsReport(){
        //call the function to intiate data
        doctor_repartition_fill();
        mutual_repartition_fill();
        salary_service_repartition_fill();
        ratio_nurse_patient_fill();
        ratio_bed_per_room_fill();
    }
    
    //Repartition of doctor by service
    private void doctor_repartition_fill(){
        ArrayList colums = new ArrayList();
        colums.add("count(no_employe), docteur.specialite");
        ArrayList tables = new ArrayList();
        tables.add("docteur");
        
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, null, "specialite", null);
        
        ArrayList<Result_line> usable = Test_request.data();
        
        //instanciation
        doctor_repartition = new ArrayList<ArrayList>();
        
        for(int i =0; i< usable.size(); i++){
            //add to the variable
            doctor_repartition.add(new ArrayList());
            doctor_repartition.get(i).add(usable.get(i).getCount_employe()); 
            doctor_repartition.get(i).add(usable.get(i).getSpecialty()); 
        }
        
        Doctor_repartition_chart = new PieChart_AWT ("Repartition of doctors by department", doctor_repartition);
        Doctor_repartition_chart.setSize(560,367);
        Doctor_repartition_panel = Doctor_repartition_chart.getChartPanel();
    }
    
    //Repartition of mutual health care in the patients
    private void mutual_repartition_fill(){
        ArrayList colums = new ArrayList();
        colums.add("count(no_malade), malade.mutuelle"); //have to precise from which table you want it
        ArrayList tables = new ArrayList();
        tables.add("malade");
        
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, null, "mutuelle", null);
        
        ArrayList<Result_line> usable = Test_request.data();
        
        //instanciation
        mutual_repartition = new ArrayList<ArrayList>();
        
        for(int i =0; i< usable.size(); i++){
            //add to the variable
            mutual_repartition.add(new ArrayList());
            mutual_repartition.get(i).add(usable.get(i).getCount_patient()); 
            mutual_repartition.get(i).add(usable.get(i).getMutual_health()); 
        }
        
        //Creation du Chart et affichage de test
        Mutual_repartition_chart = new PieChart_AWT("Repartition of patients according to mutual care", mutual_repartition);
        Mutual_repartition_chart.setSize(560,367);
        Mutual_repartition_panel = Mutual_repartition_chart.getChartPanel();
    }
    
    //Repartiiton of the salary in the different services
    private void salary_service_repartition_fill(){
        QuickSearch r5 = new QuickSearch(5);
        Creation Test_request = new Creation(r5.getChosen());
        
        ArrayList<Result_line> usable = Test_request.data();
        
        //instanciation
        salary_service_repartition = new ArrayList<ArrayList>();
        
        for(int i =0; i< usable.size(); i++){
            //add to the variable
            salary_service_repartition.add(new ArrayList());
            
            salary_service_repartition.get(i).add(usable.get(i).getDepartment_code()); 
            salary_service_repartition.get(i).add(usable.get(i).getAvg_salary()); 
        }
        
        Salary_service_repartition_chart = new BarChart_AWT("", "Nurse salary depending on department", salary_service_repartition);
        Salary_service_repartition_chart.pack();
        Salary_service_repartition_chart.setSize(560,367);
        Salary_service_repartition_panel = Salary_service_repartition_chart.getChartPanel();  
    }
    
    private void ratio_nurse_patient_fill(){
        
        ArrayList colums = new ArrayList();
        colums.add("nom_service, COUNT(DISTINCT infirmier.no_employe)/COUNT(DISTINCT hospitalisation.no_malade) as RatioNursePerPatient"); //have to precise from which table you want it
        ArrayList tables = new ArrayList();
        tables.add("infirmier");
        tables.add("service");
        tables.add("hospitalisation");
        
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, null, "service.nom_service", null);
        ArrayList<Result_line> usable = Test_request.data();
        
        //instanciation
        ratio_nurse_per_patient = new ArrayList<ArrayList>();
        
        for(int i =0; i< usable.size(); i++){
            //add to the variable
            ratio_nurse_per_patient.add(new ArrayList());
            
            ratio_nurse_per_patient.get(i).add(usable.get(i).getDepartment_name()); 
            ratio_nurse_per_patient.get(i).add(usable.get(i).getRatio_nurse_patient()); 
        }
        
        Ratio_nurse_per_patient_chart = new BarChart_AWT("", "Ratio of nurse per patients", ratio_nurse_per_patient);
        Ratio_nurse_per_patient_chart.pack();
        Ratio_nurse_per_patient_panel = Ratio_nurse_per_patient_chart.getChartPanel();
    }
    
    //Average bed per room in each service
    private void ratio_bed_per_room_fill(){
        
        ArrayList colums = new ArrayList();
        colums.add("service.nom_service, avg(nb_lits)"); //have to precise from which table you want it
        ArrayList tables = new ArrayList();
        tables.add("chambre");
        tables.add("service");
        
        Creation Test_request = new Creation(Type_Query.SELECT, colums, tables, null, null, "service.code_service", null);
        
        ArrayList<Result_line> usable = Test_request.data();
        
        //instanciation
        ratio_bed_per_room = new ArrayList<ArrayList>();
        
        for(int i =0; i< usable.size(); i++){
            //add to the variable
            ratio_bed_per_room.add(new ArrayList());
            
            ratio_bed_per_room.get(i).add(usable.get(i).getDepartment_name()); 
            ratio_bed_per_room.get(i).add(usable.get(i).getAvg_nb_bed()); 
        }
        
        ratio_bed_per_room_chart = new BarChart_AWT("", "Average number of bed per room in each service", ratio_bed_per_room);
        ratio_bed_per_room_chart.pack();
        ratio_bed_per_room_chart.setSize(560,367);
        Ratio_bed_per_room_panel = ratio_bed_per_room_chart.getChartPanel();
    }
    
    public ChartPanel getDoctor_repartition_panel(){
        return Doctor_repartition_panel;
    }

    public ChartPanel getMutual_repartition_panel(){
        return Mutual_repartition_panel;
    }

    public ChartPanel getSalary_service_repartition_panel(){
        return Salary_service_repartition_panel;
    }

    public ChartPanel getRatio_nurse_per_patient_panel() {
        return Ratio_nurse_per_patient_panel;
    }

    public ChartPanel getRatio_bed_per_room_panel() {
        return Ratio_bed_per_room_panel;
    }
}