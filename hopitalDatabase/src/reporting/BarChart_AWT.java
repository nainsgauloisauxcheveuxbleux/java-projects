/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting;

import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

/**
 *source: https://www.tutorialspoint.com/jfreechart/jfreechart_bar_chart.htm
 * @author Henri
 */
public class BarChart_AWT extends ApplicationFrame{
    
    ArrayList<ArrayList> Chart_data;
    ChartPanel chartPanel;
    
    public BarChart_AWT(String applicationTitle, String title, ArrayList<ArrayList> _Chart_data){
      super(applicationTitle);
      Chart_data = _Chart_data;
      JFreeChart barChart = ChartFactory.createBarChart(
         title,
         "Categories",         //Change to be determined   
         "Score",            //Change to be determined
         createDataset(),
         PlotOrientation.VERTICAL,
         true, true, false);
      
      chartPanel = new ChartPanel(barChart);
      chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
   }
   
   private CategoryDataset createDataset(){
      
      final DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
      
      for(int i=0; i<Chart_data.size();i++){
          dataset.addValue((float)Chart_data.get(i).get(1), (String)Chart_data.get(i).get(0), " ");
      }
      return dataset; 
   }

    public ChartPanel getChartPanel() {
        return chartPanel;
    }
   
}
