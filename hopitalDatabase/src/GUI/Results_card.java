package GUI;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;
import model.Result_line;

/**
 * Print the statement result in a Panel to be return to Main_card
 * 
 * @author Gabriel
 * @version 1.1
 */
public class Results_card extends JPanel{
    JPanel global = new JPanel();
    JScrollPane navigation;
    JPanel result;
    ArrayList<Result_line> usable;
    ArrayList<String> columns;
    
    /**
     * Create a panel with the information in the parameters
     * 
     * @param to_use
     * @param _columns 
     */
    public Results_card(ArrayList<Result_line> to_use, ArrayList<String> _columns){
        usable = to_use;
        columns = _columns;
    }
    
    /**
     * Fill the panel with data
     * 
     * @author Gabriel
     * @version 1.0
     */
    public void init(){
        result = new JPanel();
        //System.out.println(columns.size()-1);
        result.setLayout(new GridLayout(0, columns.size()));
        
        for(String col :  columns){
            JLabel lab_col = new JLabel(col + "  ");
            result.add(lab_col);
        }
        
        //Retrieve the data in out objects to print it
        for(int j = 0; j< usable.size(); j++){
            //Search_result.add(new Result_line());
            for(int i = 0; i<columns.size(); i++){
                JLabel lab = new JLabel();
                switch(columns.get(i)){
                    case "no_employe": lab.setText(Integer.toString(usable.get(j).getEmployee_id()));
                        result.add(lab);
                        break;
                    case "employe.no_employe": lab.setText(Integer.toString(usable.get(j).getEmployee_id()));
                        result.add(lab);
                        break;
                    case "docteur.no_employe": lab.setText(Integer.toString(usable.get(j).getEmployee_id()));
                        result.add(lab);
                        break;
                    case "soigne.no_employe": lab.setText(Integer.toString(usable.get(j).getEmployee_id()));
                        result.add(lab);
                        break;
                    case "chambre.no_employe": lab.setText(Integer.toString(usable.get(j).getEmployee_id()));
                        result.add(lab);
                        break;
                    case "nom_employe": lab.setText(usable.get(j).getEmployee_name());
                        result.add(lab);
                        break;
                    case "prenom_employe": lab.setText(usable.get(j).getEmployee_firstname());
                        result.add(lab);
                        break;
                    case "adresse_employe": lab.setText(usable.get(j).getEmployee_address());
                        result.add(lab);
                        break;
                    case "tel_employe": lab.setText(usable.get(j).getEmployee_phone_number());
                        result.add(lab);
                        break;
                    case "code_service": lab.setText(usable.get(j).getDepartment_code());
                        result.add(lab);
                        break;
                    case "chambre.code_service": lab.setText(usable.get(j).getDepartment_code());
                        result.add(lab);
                        break;
                    case "hospitalisation.code_service": lab.setText(usable.get(j).getDepartment_code());
                        result.add(lab);
                        break;
                    case "infirmier.code_service": lab.setText(usable.get(j).getDepartment_code());
                        result.add(lab);
                        break;
                    case "service.code_service": lab.setText(usable.get(j).getDepartment_code());
                        result.add(lab);
                        break;
                    case "rotation": lab.setText(usable.get(j).getDay_shift());
                        result.add(lab);
                        break;
                    case "salaire": lab.setText(Float.toString(usable.get(j).getSalary()));
                        result.add(lab);
                        break;
                    case "specialite": lab.setText(usable.get(j).getSpecialty());
                        result.add(lab);
                        break;
                    case "nom_service": lab.setText(usable.get(j).getDepartment_name());
                        result.add(lab);
                        break;
                    case "batiment": lab.setText(usable.get(j).getBuilding());
                        result.add(lab);
                        break;
                    case "no_chambre": lab.setText(Integer.toString(usable.get(j).getNb_room()));
                        result.add(lab);
                        break;
                    case "chambre.no_chambre": lab.setText(Integer.toString(usable.get(j).getNb_room()));
                        result.add(lab);
                        break;
                    case "hospitalisation.no_chambre": lab.setText(Integer.toString(usable.get(j).getNb_room()));
                        result.add(lab);
                        break;
                    case "no_lit": lab.setText(Integer.toString(usable.get(j).getId_bed()));
                        result.add(lab);
                        break;
                    case "nb_lits": lab.setText(Integer.toString(usable.get(j).getNb_bed()));
                        result.add(lab);
                        break;
                    case "no_malade": lab.setText(Integer.toString(usable.get(j).getPatient_id()));
                        result.add(lab);
                        break;
                    case "malade.no_malade": lab.setText(Integer.toString(usable.get(j).getPatient_id()));
                        result.add(lab);
                        break;
                    case "hospitalisation.no_malade": lab.setText(Integer.toString(usable.get(j).getPatient_id()));
                        result.add(lab);
                        break;    
                    case "soigne.no_malade": lab.setText(Integer.toString(usable.get(j).getPatient_id()));
                        result.add(lab);
                        break;
                    case "nom_malade": lab.setText(usable.get(j).getPatient_name());
                        result.add(lab);
                        break;
                    case "prenom_malade": lab.setText(usable.get(j).getPatient_firstname());
                        result.add(lab);
                        break;
                    case "adresse_malade": lab.setText(usable.get(j).getPatient_address());
                        result.add(lab);
                        break;
                    case "tel_malade": lab.setText(usable.get(j).getPatient_phone_number());
                        result.add(lab);
                        break;
                    case "mutuelle": lab.setText(usable.get(j).getMutual_health());
                        result.add(lab);
                        break;
                    
                    //Counts, averages and ratios
                    case "count(no_employe)":lab.setText(Integer.toString(usable.get(j).getCount_employe()));
                        result.add(lab);
                        break;
                    case "count(distinct no_employe)":lab.setText(Integer.toString(usable.get(j).getCount_employe()));
                        result.add(lab);
                        break;
                    case "count(no_chambre)":lab.setText(Integer.toString(usable.get(j).getCount_room()));
                        result.add(lab);
                        break;
                    case "count(no_malade)":lab.setText(Integer.toString(usable.get(j).getCount_patient()));
                        result.add(lab);
                        break;
                    case "count(specialite)":lab.setText(Integer.toString(usable.get(j).getCount_specialty()));
                        result.add(lab);
                        break;
                    case "NbSpec":lab.setText(Integer.toString(usable.get(j).getCount_specialty()));
                        result.add(lab);
                        break;
                    case "NbDoctor":lab.setText(Integer.toString(usable.get(j).getCount_doctor()));
                        result.add(lab);
                        break;
                    case "avg(salaire)":lab.setText(Float.toString(usable.get(j).getAvg_salary()));
                        result.add(lab);
                        break;
                    case "avg(nb_lits)":lab.setText(Float.toString(usable.get(j).getAvg_nb_bed()));
                        result.add(lab);
                        break;
                    case "RatioNursePerPatient":lab.setText(Float.toString(usable.get(j).getRatio_nurse_patient()));
                        result.add(lab);
                        break;
                }
            }
        }
        
        navigation = new JScrollPane(result, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        global.add(navigation);
    }
    
    /**
     * Return the filled panel
     * 
     * @return JPanel 
     */
    public JPanel getGlobal(){
        return global;
    }
}
