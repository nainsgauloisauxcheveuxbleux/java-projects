package GUI;

import controler.Creation;
import controler.QuickSearch;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import model.Result_line;

/**
 * Quick Search for the stored statement
 * 
 * @author arthur, Gabriel
 */
public class R_card extends JPanel implements ActionListener{
    private final JPanel cards;
    
    private final JButton r1_button = new JButton("R1");
    private final JButton r2_button = new JButton("R2");
    private final JButton r3_button = new JButton("R3");
    private final JButton r4_button = new JButton("R4");
    private final JButton r5_button = new JButton("R5");
    private final JButton r6_button = new JButton("R6");
    private final JButton r7_button = new JButton("R7");
    private final JButton r8_button = new JButton("R8");
    private final JButton r9_button = new JButton("R9");
    private final JButton r10_button = new JButton("R10");
    
    private final JPanel panel1_5 = new JPanel();
    private final JPanel panel6_10 = new JPanel();
    private final JPanel panel_return = new JPanel();
    private JPanel results_panel = new JPanel();
    
    private final JButton return_button = new JButton("Return to main menu");
    
    private QuickSearch r;
    private ArrayList<String> columns;
    Results_card res_card;
    
    ArrayList<Result_line> result_r_statement;
    /**
     * Create the button and place them
     * 
     * @param _cards 
     * 
     * @author arhtur, Gabriel
     */
    public R_card(JPanel _cards){
        super();
        cards = _cards;
        panel1_5.add(r1_button);
        panel1_5.add(r2_button);
        panel1_5.add(r3_button);
        panel1_5.add(r4_button);
        panel1_5.add(r5_button);
        
        panel6_10.add(r6_button);
        panel6_10.add(r7_button);
        panel6_10.add(r8_button);
        panel6_10.add(r9_button);
        panel6_10.add(r10_button);
        
        panel_return.add(return_button);
        
    }
    
    /**
     * Add the action listeners
     * 
     * @author arthur, Gabriel
     */
    public void init(){
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        r1_button.addActionListener((ActionListener) this);
        r2_button.addActionListener((ActionListener) this);
        r3_button.addActionListener((ActionListener) this);
        r4_button.addActionListener((ActionListener) this);
        r5_button.addActionListener((ActionListener) this);
        r6_button.addActionListener((ActionListener) this);
        r7_button.addActionListener((ActionListener) this);
        r8_button.addActionListener((ActionListener) this);
        r9_button.addActionListener((ActionListener) this);
        r10_button.addActionListener((ActionListener) this);
        return_button.addActionListener((ActionListener) this);
        
        
        
        this.add(Box.createRigidArea(new Dimension(0,100)));
        JPanel temp = new JPanel();
        temp.setLayout(new BoxLayout(temp, BoxLayout.PAGE_AXIS));
        temp.add(panel1_5);
        temp.add(panel6_10);
        this.setLayout(new BorderLayout());
        this.add(temp, BorderLayout.NORTH);
        this.add(results_panel, BorderLayout.CENTER);
        this.add(panel_return, BorderLayout.SOUTH);
        this.setVisible(true);
    }
    
    /**
     * Action Perform which call the corresponding QuickSearch for the pressed button
     * 
     * @param e 
     */
    @Override
     public void actionPerformed(ActionEvent e) {
        results_panel.removeAll();
         
        if(e.getSource()==r1_button){
            //retrieve the corresponding query
            r = new QuickSearch(1);
            //and the list of it's fields
            columns = r.getColumn(1);
            
            //apply the sql
            Creation Test_request = new Creation(r.getChosen());
            //get the result
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r2_button)
        {
            r = new QuickSearch(2);
            columns = r.getColumn(2);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r3_button)
        {
            r = new QuickSearch(3);
            columns = r.getColumn(3);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r4_button)
        {
            r = new QuickSearch(4);
            columns = r.getColumn(4);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r5_button)
        {
            r = new QuickSearch(5);
            columns = r.getColumn(5);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r6_button)
        {
            r = new QuickSearch(6);
            columns = r.getColumn(6);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r7_button)
        {
            r = new QuickSearch(7);
            columns = r.getColumn(7);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r8_button)
        {
            r = new QuickSearch(8);
            columns = r.getColumn(8);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r9_button)
        {
            r = new QuickSearch(9);
            columns = r.getColumn(9);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==r10_button)
        {
            r = new QuickSearch(10);
            columns = r.getColumn(10);
            Creation Test_request = new Creation(r.getChosen());
            result_r_statement = Test_request.data();
        }
        else if(e.getSource()==return_button)
        {
            CardLayout cl;
            cl = (CardLayout)(cards.getLayout());
            cl.first(cards);
        }
        
        //send the result to be treated and added to a panel
        res_card = new Results_card(result_r_statement, columns);
        res_card.init();
        //show the obtained result
        results_panel.add(res_card.getGlobal());
        
        //actualization
        this.revalidate();
        this.repaint();
    }
}
