/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcv2018.Connexion;

/**
 * Implementation of some pre-made statement to run faster in the bdd
 * 
 * @author Gabriel
 */
public class QuickSearch {
    
    //Variables
    private ArrayList<String> R = new ArrayList<>();
    private ArrayList<ArrayList<String>> columns;
    
    private ArrayList<String> column_name;
    private ArrayList<ArrayList<String>> result;
    
    private String chosen;
    
    //Constructor
    /**
     * Fill the R AL with all the prepared R request
     * Then execute the corresponding request for the given parameter in the constructor
     * 
     * add the corresponding tables to each R statement
     * 
     * @param r_number 
     * @author Gabriel, Henri
     * source : Alexis Martin (R7)
     */
    public QuickSearch(int r_number) {
        this.columns = new ArrayList<ArrayList<String>>();
        
        
        /**
        * Je vais créer pleins de requêtes plus ou moins random pour m'entrainer et m'assurer du bon fonctionnement de ce qu'on va créer avec l'interface
        * Ca servira aussi de backup si jamais tout foire dans la création de requêtes modulaires...
        * @author Henri
        */
        R.add("SELECT prenom_malade, nom_malade FROM malade WHERE mutuelle = 'MAAF'");//R1
        columns.add(new ArrayList<String>());
        columns.get(0).add("prenom_malade");
        columns.get(0).add("nom_malade");
        
        R.add("SELECT employe.nom_employe, employe.prenom_employe "
                + "FROM employe INNER JOIN infirmier on infirmier.no_employe = employe.no_employe "
                + "WHERE rotation = 'NUIT'");//R2
        columns.add(new ArrayList<String>());
        columns.get(1).add("nom_employe");
        columns.get(1).add("prenom_employe");
        
        R.add("SELECT service.nom_service, service.batiment, employe.prenom_employe, employe.nom_employe, docteur.specialite "
                + "FROM service INNER JOIN docteur on service.directeur = docteur.no_employe "
                + "INNER JOIN employe on docteur.no_employe = employe.no_employe");//R3
        columns.add(new ArrayList<String>());
        columns.get(2).add("nom_service");
        columns.get(2).add("batiment");
        columns.get(2).add("prenom_employe");
        columns.get(2).add("nom_employe");
        columns.get(2).add("specialite");
        
        R.add("SELECT hospitalisation.no_chambre, hospitalisation.no_lit, service.nom_service, malade.prenom_malade, malade.nom_malade, malade.mutuelle "
                + "FROM hospitalisation INNER JOIN malade ON hospitalisation.no_malade = malade.no_malade "
                + "INNER JOIN service ON hospitalisation.code_service = service.code_service "
                + "WHERE mutuelle LIKE 'MN%' AND batiment = 'B'");//R4
        columns.add(new ArrayList<String>());
        columns.get(3).add("no_chambre");
        columns.get(3).add("no_lit");
        columns.get(3).add("nom_service");
        columns.get(3).add("prenom_malade");
        columns.get(3).add("nom_malade");
        columns.get(3).add("mutuelle");
        
        R.add("SELECT avg(salaire), code_service FROM infirmier GROUP BY code_service");//R5
        columns.add(new ArrayList<String>());
        columns.get(4).add("avg(salaire)");
        columns.get(4).add("code_service");
        
        R.add("SELECT chambre.code_service, avg(nb_lits) FROM chambre "
                + "INNER JOIN service ON chambre.code_service = service.code_service WHERE batiment = 'A' GROUP BY code_service");//R6
        columns.add(new ArrayList<String>());
        columns.get(5).add("code_service");
        columns.get(5).add("avg(nb_lits)");
        
        R.add("SELECT malade.nom_malade, malade.prenom_malade, COUNT(docteur.no_employe) as NbDoctor, COUNT(distinct specialite) as NbSpec "
                + "FROM malade INNER JOIN soigne ON malade.no_malade = soigne.no_malade " 
                + "INNER JOIN docteur ON docteur.no_employe = soigne.no_employe "
                + "GROUP BY malade.no_malade "
                + "HAVING COUNT(soigne.no_employe)>3");//R7 source : Alexis Martin
        columns.add(new ArrayList<String>());
        columns.get(6).add("nom_malade");
        columns.get(6).add("prenom_malade");
        columns.get(6).add("NbDoctor");
        columns.get(6).add("NbSpec");
        
        R.add("SELECT service.nom_service, COUNT(DISTINCT infirmier.no_employe)/COUNT(DISTINCT hospitalisation.no_malade) as RatioNursePerPatient "
                + "FROM service INNER JOIN infirmier ON service.code_service = infirmier.code_service "
                + "INNER JOIN hospitalisation ON service.code_service = hospitalisation.code_service "
                + "GROUP BY service.code_service"); //R8
        columns.add(new ArrayList<String>());
        columns.get(7).add("nom_service");
        columns.get(7).add("RatioNursePerPatient");
        
        R.add("SELECT employe.prenom_employe, employe.nom_employe "
                + "FROM employe WHERE employe.no_employe IN(SELECT no_employe FROM soigne "
                + "INNER JOIN hospitalisation ON soigne.no_malade = hospitalisation.no_malade)");//R9
        columns.add(new ArrayList<String>());
        columns.get(8).add("prenom_employe");
        columns.get(8).add("nom_employe");
        
        R.add("SELECT employe.prenom_employe, employe.nom_employe "
                + "FROM employe INNER JOIN docteur ON employe.no_employe = docteur.no_employe "
                + "WHERE employe.no_employe NOT IN(SELECT no_employe FROM soigne INNER JOIN hospitalisation ON soigne.no_malade = hospitalisation.no_malade)");//R10
        columns.add(new ArrayList<String>());
        columns.get(9).add("prenom_employe");
        columns.get(9).add("nom_employe");
        
        //exectue the request R for the given number
        if(r_number >= 1 && r_number <= 10) try {
            execute_search(r_number - 1); //R from 1-10 but ArrayList from 0-9
            chosen = R.get(r_number - 1);
        } catch (SQLException ex) {
            Logger.getLogger(QuickSearch.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(QuickSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //getters
    public ArrayList<String> get_column_name(){
        return column_name;
    }
    
    public ArrayList<ArrayList<String>> get_result(){
        return result;
    }
    
    public String getChosen(){
        return chosen;
    }
    
    public ArrayList<String> getColumn(int a){
        return columns.get(a-1);
    }
    
    //methods
    /**
     * Take as a parameter the R function to execute and stock the results in column_name for the name of the SELECT and the resutls in result
     * 
     * @param index
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    private void execute_search(int index) throws SQLException, ClassNotFoundException{
        
        //local
        Connexion local = new Connexion("hopital", "root", "");
        
        //column name
        ArrayList<ArrayList<String>> global_AL = null;
        try {
        
            global_AL = local.remplirChampsTable(R.get(index));
            
            column_name = global_AL.get(0);
        
            //result
            result = local.remplirChampsRequete(R.get(index));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(QuickSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
