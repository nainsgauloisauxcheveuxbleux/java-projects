/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.util.ArrayList;

/**
 * Small class to help in multi-inner join
 * 
 * @author Gabriel
 */
public class NameFrom {
    public String column_innerjoin_name;
    public ArrayList<String> table_innerjoin_name;
    
    /**
     *
     * @param column
     * @param table1
     * @param table2
     */
    public NameFrom(String column, String table1, String table2){
        table_innerjoin_name = new ArrayList();
        
        column_innerjoin_name = column;
        //System.out.println("table 1 :" + table1);
        table_innerjoin_name.add(table1);
        //System.out.println("table 2 :" + table2);
        table_innerjoin_name.add(table2);
    }
}
