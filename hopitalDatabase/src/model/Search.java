/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import model.*;

/**
 * Extract the data into an ArrayList of objects to be more useful
 * 
 * @author Henri
 */
public class Search {
   
   private ArrayList <String> fields_table;//Chaque String correspond à un champs rendu par la requête SQL (une colonne)
   private ArrayList<ArrayList<String>> SQL_result_table;//Conteneur de résultat fourni à la suite de la requête SQL
   private ArrayList <Result_line> Search_result; //Conteneur des résultats sous leur forme utilisable par l'interface 
    
    public Search() {

    }
    
    public Search(ArrayList<String> _fields_table, ArrayList<ArrayList<String>> _SQL_result_table) {
        fields_table = _fields_table;
        SQL_result_table = _SQL_result_table;
    }
   /**
    * Depending on the names of the fields of the SQL request to be treated, information from the result of the request is set with the tight data type
    * into a Result_line object which will be used by the interface
    * @version 1.1
    * @author Henri
    */
    public ArrayList extract_result(){
        Search_result = new ArrayList<Result_line>();
        for(int j = 0; j< SQL_result_table.size(); j++){
            Search_result.add(new Result_line());
            for(int i = 0; i<fields_table.size(); i++){
                switch(fields_table.get(i)){
                    case "no_employe": Search_result.get(j).setEmployee_id(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "nom_employe": Search_result.get(j).setEmployee_name(SQL_result_table.get(j).get(i)); break;
                    case "prenom_employe": Search_result.get(j).setEmployee_firstname(SQL_result_table.get(j).get(i)); break;
                    case "adresse_employe": Search_result.get(j).setEmployee_address(SQL_result_table.get(j).get(i)); break;
                    case "tel_employe": Search_result.get(j).setEmployee_phone_number(SQL_result_table.get(j).get(i)); break;
                    case "code_service": Search_result.get(j).setDepartment_code(SQL_result_table.get(j).get(i)); break;
                    case "rotation": Search_result.get(j).setDay_shift(SQL_result_table.get(j).get(i)); break;
                    case "salaire": Search_result.get(j).setSalary(Float.parseFloat(SQL_result_table.get(j).get(i))); break;
                    case "specialite": Search_result.get(j).setSpecialty(SQL_result_table.get(j).get(i)); break;
                    case "nom_service": Search_result.get(j).setDepartment_name(SQL_result_table.get(j).get(i)); break;
                    case "batiment": Search_result.get(j).setBuilding(SQL_result_table.get(j).get(i)); break;
                    case "directeur": Search_result.get(j).setDirector_id(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "no_chambre": Search_result.get(j).setNb_room(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "no_lit": Search_result.get(j).setId_bed(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "nb_lits": Search_result.get(j).setNb_bed(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "no_malade": Search_result.get(j).setPatient_id(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "nom_malade": Search_result.get(j).setPatient_name(SQL_result_table.get(j).get(i)); break;
                    case "prenom_malade": Search_result.get(j).setPatient_firstname(SQL_result_table.get(j).get(i)); break;
                    case "adresse_malade": Search_result.get(j).setPatient_address(SQL_result_table.get(j).get(i)); break;
                    case "tel_malade": Search_result.get(j).setPatient_phone_number(SQL_result_table.get(j).get(i)); break;
                    case "mutuelle": Search_result.get(j).setMutual_health(SQL_result_table.get(j).get(i)); break;
                    
                    //Counts, averages and ratios
                    case "count(no_employe)":Search_result.get(j).setCount_employe(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "count(distinct no_employe)":Search_result.get(j).setCount_employe(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "count(no_chambre)":Search_result.get(j).setCount_room(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "count(no_malade)":Search_result.get(j).setCount_patient(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "NbDoctor":Search_result.get(j).setCount_doctor(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "count(specialite)":Search_result.get(j).setCount_specialty(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "NbSpec":Search_result.get(j).setCount_specialty(Integer.parseInt(SQL_result_table.get(j).get(i))); break;
                    case "avg(salaire)":Search_result.get(j).setAvg_salary(Float.parseFloat(SQL_result_table.get(j).get(i))); break;
                    case "avg(nb_lits)":Search_result.get(j).setAvg_nb_bed(Float.parseFloat(SQL_result_table.get(j).get(i))); break;
                    case "RatioNursePerPatient":Search_result.get(j).setRatio_nurse_patient(Float.parseFloat(SQL_result_table.get(j).get(i))); break;
                }
            }
        }
        return Search_result;
    }
}