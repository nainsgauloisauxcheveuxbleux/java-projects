/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * This class contains all possible information given by the database on one given line
 * A container of this class will represent the entirety of a SQL request result
 * 
 * @author Henri
 * @version 1.2
 * Still some fields to add depending on how far the project goes
 */
public class Result_line {
    
    //Department related fields
    private String department_code;
    private String department_name;
    private String building;
    private int director_id;

    public String getDepartment_code(){
        return department_code;
    }

    public String getDepartment_name(){
        return department_name;
    }

    public String getBuilding(){
        return building;
    }

    public int getDirector_id() {
        return director_id;
    }
    
    //Employees, Nurse and Doctors related fields 
    private int employee_id;
    private String employee_name;
    private String employee_firstname;
    private String employee_address;
    private String employee_phone_number;
    private String day_shift;
    private float salary;
    private String specialty;

    public int getEmployee_id() {
        return employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public String getEmployee_firstname() {
        return employee_firstname;
    }

    public String getEmployee_address() {
        return employee_address;
    }

    public String getEmployee_phone_number() {
        return employee_phone_number;
    }

    public String getDay_shift() {
        return day_shift;
    }

    public float getSalary() {
        return salary;
    }

    public String getSpecialty() {
        return specialty;
    }
    
    //Hospitalization related fields
    private int patient_id;
    private String patient_name;
    private String patient_firstname;
    private String patient_address;
    private String patient_phone_number;
    private String mutual_health;
    private int nb_room;
    private int nb_bed;
    private int id_bed;
    private int id_supervisor;

    public int getPatient_id() {
        return patient_id;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public String getPatient_firstname() {
        return patient_firstname;
    }

    public String getPatient_address() {
        return patient_address;
    }

    public String getPatient_phone_number() {
        return patient_phone_number;
    }

    public String getMutual_health() {
        return mutual_health;
    }

    public int getNb_room() {
        return nb_room;
    }

    public int getNb_bed() {
        return nb_bed;
    }

    public int getId_bed() {
        return id_bed;
    }

    public int getId_supervisor() {
        return id_supervisor;
    }
    
    //Various statistics
    private int count_patient;
    private int count_employe;
    private int count_doctor;
    private int count_nurse;
    private int count_bed;
    private int count_room;
    private int count_service;
    private int count_specialty;
    
    private float avg_salary;
    private float avg_nb_bed;
    private float ratio_nurse_patient;
    
    public int getCount_patient() {
        return count_patient;
    }

    public int getCount_employe() {
        return count_employe;
    }

    public int getCount_doctor() {
        return count_doctor;
    }

    public int getCount_nurse() {
        return count_nurse;
    }

    public int getCount_bed() {
        return count_bed;
    }

    public int getCount_room() {
        return count_room;
    }

    public int getCount_service() {
        return count_service;
    }
    
    public int getCount_specialty() {
        return count_specialty;
    }

    public float getAvg_salary() {
        return avg_salary;
    }

    public float getAvg_nb_bed() {
        return avg_nb_bed;
    }

    public float getRatio_nurse_patient() {
        return ratio_nurse_patient;
    }

    public Result_line() {
    }

    public void setEmployee_address(String employee_address) {
        this.employee_address = employee_address;
    }

    public void setDepartment_code(String department_code) {
        this.department_code = department_code;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public void setDirector_id(int director_id) {
        this.director_id = director_id;
    }
    
    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public void setEmployee_firstname(String employee_firstname) {
        this.employee_firstname = employee_firstname;
    }

    public void setEmployee_phone_number(String employee_phone_number) {
        this.employee_phone_number = employee_phone_number;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public void setDay_shift(String day_shift) {
        this.day_shift = day_shift;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setId_supervisor(int id_supervisor) {
        this.id_supervisor = id_supervisor;
    }

    public void setNb_bed(int nb_bed) {
        this.nb_bed = nb_bed;
    }

    public void setNb_room(int nb_room) {
        this.nb_room = nb_room;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public void setPatient_firstname(String patient_firstname) {
        this.patient_firstname = patient_firstname;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public void setPatient_address(String patient_address) {
        this.patient_address = patient_address;
    }

    public void setPatient_phone_number(String patient_phone_number) {
        this.patient_phone_number = patient_phone_number;
    }

    public void setMutual_health(String mutual_health) {
        this.mutual_health = mutual_health;
    }

    public void setId_bed(int id_bed) {
        this.id_bed = id_bed;
    }

    public void setCount_patient(int count_patient) {
        this.count_patient = count_patient;
    }

    public void setCount_employe(int count_employe) {
        this.count_employe = count_employe;
    }

    public void setCount_doctor(int count_doctor) {
        this.count_doctor = count_doctor;
    }

    public void setCount_nurse(int count_nurse) {
        this.count_nurse = count_nurse;
    }

    public void setCount_bed(int count_bed) {
        this.count_bed = count_bed;
    }

    public void setCount_room(int count_room) {
        this.count_room = count_room;
    }

    public void setCount_service(int count_service) {
        this.count_service = count_service;
    }
    
    public void setCount_specialty(int count_speciality){
        this.count_specialty = count_speciality;
    }

    public void setAvg_salary(float avg_salary) {
        this.avg_salary = avg_salary;
    }

    public void setAvg_nb_bed(float avg_nb_bed) {
        this.avg_nb_bed = avg_nb_bed;
    }

    public void setRatio_nurse_patient(float ratio_nurse_patient) {
        this.ratio_nurse_patient = ratio_nurse_patient;
    }
}
